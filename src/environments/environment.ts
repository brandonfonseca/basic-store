// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  functions: {
    users: {
      readByUID: 'http://localhost:5000/bs-bif/us-central1/readByUID',
      readAllUsers: 'http://localhost:5000/bs-bif/us-central1/readAllUsers',
      createUser: 'http://localhost:5000/bs-bif/us-central1/createUser',
      updateUser: 'http://localhost:5000/bs-bif/us-central1/updateUser',
      deleteUser: 'http://localhost:5000/bs-bif/us-central1/deleteUser'
    },
    products:{
      bulkData: 'http://localhost:5000/bs-bif/us-central1/bulkData'
    },
    transactions: {
      transactionsPerPagination: ''
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
