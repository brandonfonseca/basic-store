export const environment = {
  production: true,
  functions: {
    users: {
      readByUID: 'https://readbyuid-idol36mnwa-uc.a.run.app',
      readAllUsers: 'https://readallusers-idol36mnwa-uc.a.run.app',
      createUser: 'https://createuser-idol36mnwa-uc.a.run.app',
      updateUser: 'https://updateuser-idol36mnwa-uc.a.run.app',
      deleteUser: 'https://deleteuser-idol36mnwa-uc.a.run.app'
    },
    products:{
      bulkData: 'https://bulkdata-idol36mnwa-uc.a.run.app'
    },
    transactions: {
      transactionsPerPagination: ''
    }
  }
};
