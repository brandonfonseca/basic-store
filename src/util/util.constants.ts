export const utilConstants = {
  noPhoto:
    'https://firebasestorage.googleapis.com/v0/b/bs-bif.appspot.com/o/assets%2Fblank-profile-picture-1280.png?alt=media&token=8a17c172-bdaf-4574-ab2a-f772c933ffdb',
  noProductPhoto:
    'https://firebasestorage.googleapis.com/v0/b/bs-bif.appspot.com/o/assets%2Fno-photo-available.png?alt=media&token=03cfceb4-73e2-402c-8d36-cba7ff7cac18',
  regexPatterns: {
    password: '^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{8,}$',
    dni: '^([0-9]{3})-([0-2][0-9]|3[0-1])([0-1]{1}[0-2]{1})([0-9]{2})-([0-9]{4})([A-Za-z])$',
  },
};
