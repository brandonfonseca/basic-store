import { FormGroup } from '@angular/forms';

/**Validate a Form Control if has errors
 *
 * @param formControlName control to validate
 * @param formGroup parenGroup
 * @param validations custom messages by error
 * @returns empty string if has not errors. Return an error custom message if there
 */
export function validateFormControlErrors(
  formControlName: string,
  formGroup: FormGroup,
  validations: Object
): string {
  const errors = formGroup.get(formControlName)?.errors;

  if (!errors) return '';

  const errorToShow = Object.entries(validations).filter(
    (v) => v[0] === Object.keys(errors)[0]
  );

  return errorToShow[0][1];
}
