import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenubarComponent } from './components/menubar/menubar.component';
import { IonicModule } from '@ionic/angular';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { AttachFileComponent } from './components/attach-file/attach-file.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [MenubarComponent, WelcomeComponent, AttachFileComponent],
  imports: [CommonModule, IonicModule],
  exports: [MenubarComponent, WelcomeComponent, AttachFileComponent],
})
export class SharedModule { }
