import { Timestamp } from "@angular/fire/firestore";

export default interface TransactionModel {
  id: string;
  module: Modules;
  docType: DocTypes;
  nature: 'ENTRADA' | 'SALIDA';
  details: {
    quantity: number;
    dollarExchange: number;
    costAvg: number;
  }
  issueDate: Timestamp;
  userEmitter: {
    name: string;
    email: string;
  }
}

export enum Modules {
  inventory,
  billing
}

export enum DocTypes {
  buy = 'COMPRA',
  sale = 'VENTA',
  discard = 'DESCARTE',
  annulment = 'ANULACIÓN',

}
