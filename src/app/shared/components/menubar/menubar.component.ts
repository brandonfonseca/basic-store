import { Component, Input, OnInit } from '@angular/core';
import { Auth, User } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { IonMenu } from '@ionic/angular';

@Component({
  selector: 'c-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.scss'],
})
export class MenubarComponent implements OnInit {
  @Input() contentId: string = '';
  @Input() menuTitle: string = 'Menu';
  @Input() menuListItemHeader: string = '';
  @Input() menuListItems: any[] = [];
  @Input() menubarTitle: string = 'Título';
  currentUser: User | null = null;
  noPhoto =
    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png';

  constructor(private router: Router, private auth: Auth) { }

  async ngOnInit() {
    await this.auth.authStateReady();
    this.currentUser = this.auth.currentUser;
  }

  goToDirection(route: string, menu: IonMenu) {
    if (route.length === 0) return;
    menu.close();
    this.router.navigateByUrl(route);
  }

  goToAccount() {
    this.router.navigate(['account']);
  }

  goBack(menu: IonMenu) {
    menu.close();
    this.router.navigate(['home']);
  }
}
