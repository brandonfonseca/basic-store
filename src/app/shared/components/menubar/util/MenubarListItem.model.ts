export default interface MenubarListItemModel {
  icon?: string;
  title: string;
  description?: string;
  route: string;
}
