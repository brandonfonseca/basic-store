import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { AttachFileComponent } from './attach-file.component';
import { SharedModule } from '../../shared.module';
import { ReactiveFormsModule } from '@angular/forms';

xdescribe('AttachFileComponent', () => {
  let component: AttachFileComponent;
  let fixture: ComponentFixture<AttachFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AttachFileComponent],
      providers: [ModalController],
      imports: [
        SharedModule,
        ReactiveFormsModule,
        IonicModule.forRoot(),
        // provideFirebaseApp(() =>
        //   initializeApp({
        //     apiKey: 'AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs',
        //     authDomain: 'bs-bif.firebaseapp.com',
        //     projectId: 'bs-bif',
        //     storageBucket: 'bs-bif.appspot.com',
        //     messagingSenderId: '389025844621',
        //     appId: '1:389025844621:web:0e69097f5c33e9ea9381c4',
        //   })
        // ),
        // provideAuth(() => getAuth()),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
