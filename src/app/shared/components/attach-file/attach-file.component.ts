import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
// import { AttachService } from './attach.service';

@Component({
  selector: 'c-attach-file',
  templateUrl: './attach-file.component.html',
  styleUrls: ['./attach-file.component.scss'],
})
export class AttachFileComponent implements OnInit {

  fileSelected: File | undefined;

  constructor(
    private modalCtrl: ModalController,
    // private attachService: AttachService
  ) { }

  ngOnInit() { }

  onFileUp(event: DragEvent) {
    event.preventDefault();


    this.fileSelected = event.dataTransfer?.files[0];

    console.log('Subiendo Archivo...', this.fileSelected);
  }

  openFileChooser(input: HTMLInputElement) {
    input.click();
  }

  onFileSelected(event: Event) {
    const input = event.target as HTMLInputElement;

    this.fileSelected = input.files?.item(0) as File;

    console.log(this.fileSelected);

  }

  async onFileSend() {
    console.log("Enviando archivo...");

    const form = new FormData();

    form.set('excel', this.fileSelected as File);

    console.log(form.get('excel'));
    console.log(form);



    // await this.attachService.sendFile(form);

  }

  onCancel() {
    console.log("Canceling file sending...");
    this.modalCtrl.dismiss();
  }

  handleDragOver(event: Event) {
    event.preventDefault();
  }

}
