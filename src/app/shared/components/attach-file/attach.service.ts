import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getFunctions, httpsCallableFromURL } from '@angular/fire/functions';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AttachService {

  constructor(
    private http: HttpClient
  ) { }


  async sendFile(form: FormData) {

    const functions = getFunctions();
    const bulkDataFunction = httpsCallableFromURL<FormData, any>(
      functions,
      environment.functions.products.bulkData
    )

    this.http.post(environment.functions.products.bulkData, form).subscribe(data => console.log(data));


    // // const callData = httpsCallableFromURL(functions, 'http://localhost:5000/bs-bif/us-central1/callData');

    // const result = await bulkDataFunction(
    //   form
    // )

    // console.log(result);

  }

}
