export const modules = [
  {
    "code": "billing",
    "name": "Facturación",
    "articles": [
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Descripción del Módulo",
        "text": "El módulo de facturación brinda al usuario funcionalidades para registrar las ventas realizadas en el negocio, así como las facturas, control de artículos vendidos y registro de ventas manualmente para un mayor control."
      },
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Registro de ventas",
        "text": "Las ventas se pueden realizar mediante el menú de opciones, seleccionando la opción punto de venta, este provee una interfaz para agregar productos a la factura, calcular totales y facturar. El resultado es una venta la cuál se puede consultar en la opción Registro de ventas"
      },
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Registro de ventas manuales",
        "text": "El sistema le permite agregar todos los registros de ventas que usted posee, ya sea en papel o en otro sistema, esto lo puede realizar en la opción Registro de ventas"
      }
    ]
  },
  {
    "code": "administration",
    "name": "Administración",
    "articles": [
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Descripción del Módulo",
        "text": "En el módulo de administración podrás tener el control de los usuarios que tienen acceso al sistema, puedes agregar, actualizar, o eliminar los usuarios cuando lo consideres conveniente."
      },
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Registro de usuarios",
        "text": "Como administrador tienes la libertad en el sistema para la creación de nuevos usuarios y asignar sus roles (incluyendo otros administradores), tienes control total sobre ellos y puedes eliminar o deshabilitarlos cuando consideres conveniente"
      },
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Superusuario",
        "text": "El súperusuario es un usuario a nivel de infraestructura de sistema, esto significa que este usuario es únicamente para el administrador del software a nivel superior, pudiendo hacer operaciones en la base de datos del sistema sin ingresar a él."
      }
    ]
  },
  {
    "code": "inventory",
    "name": "Inventario",
    "articles": [
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Descripción del Módulo",
        "text": "El módulo de inventarios te permite poder registrar los productos de tu negocio y añadir existencias de los pedidos que realices."
      },
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Añade solo lo nuevo",
        "text": "Al tener un producto registrado en la base de datos del sistema, puedes agregar las nuevas existencias que lleguen solo usando el código del producto (También puedes cambiar el precio)"
      },
      {
        "picture": "https://assets-blog.hostgator.mx/wp-content/uploads/2021/10/cuando-se-utilizan-las-imagenes-vectoriales.webp",
        "title": "Consula solo lo que necesitas",
        "text": "Con el fin de agilizar tu búsqueda de información sobre los productos, puedes usar las herramientas de búsqueda, así como las de paginado de inventario para consultar solo lo que necesites saber"
      }
    ]
  }
]
