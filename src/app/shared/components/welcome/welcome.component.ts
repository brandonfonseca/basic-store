import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { modules } from './welcome.util';

@Component({
  selector: 'c-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit {
  title = 'Módulo'
  articles: any[] = [];

  constructor(private routeSnapshot: ActivatedRoute) { }

  ngOnInit() {
    this.routeSnapshot.data.subscribe(d => {
      const { module, articles } = d;
      this.articles = articles;
      console.log(module, articles);


      const activeModule = modules.find(m => module === m.code);
      this.title = activeModule?.name || 'Sin Nombre';
      this.articles = activeModule?.articles || [];


      console.log(activeModule);


    })


  }

  scrollTo(id: string | number) {
    const idConverted = id as string;

    const element = document.getElementById(idConverted);
    const parentScroll = document.getElementById('grid-content');

    parentScroll?.scrollTo({ behavior: 'smooth', top: element?.offsetTop });

    console.log('Scrolling to', id);
  }
}
