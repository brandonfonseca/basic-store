import { Injectable } from '@angular/core';
import { getFunctions, httpsCallableFromURL } from '@angular/fire/functions';
import { environment } from 'src/environments/environment';
import { Modules } from '../types/transactions.type';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor() { }

  async getTransactionsByPagination(elementsPerPage: number) {

    const functions = getFunctions();

    const getTransactions = httpsCallableFromURL(
      functions,
      environment.functions.transactions.transactionsPerPagination
    )

    const result = await getTransactions({
      elementsPerPage
    })

    return result;
  }

  async getTransactionsByPaginations(elementsPerPage: number, module: Modules) {
    const functions = getFunctions();

    const getTransactions = httpsCallableFromURL(
      functions,
      environment.functions.transactions.transactionsPerPagination
    )

    const result = await getTransactions({
      elementsPerPage,
      module
    })

    return result;
  }

}
