import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard'
import { adminGuard } from './guards/role.guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['auth']);

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomePageModule),
    canActivate: [AuthGuard],
    data: {
      authGuardPipe: redirectUnauthorizedToLogin
    }
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./pages/auth/auth.module').then((m) => m.AuthPageModule),
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./pages/account/account.module').then((m) => m.AccountPageModule),
    canActivate: [AuthGuard],
    data: {
      authGuardPipe: redirectUnauthorizedToLogin
    }
  },
  {
    path: 'module1',
    loadChildren: () =>
      import('./pages/modules/module1/module1.module').then(
        (m) => m.Module1PageModule
      ),
  },
  {
    path: 'administration',
    loadChildren: () =>
      import('./pages/modules/administration/administration.module').then(
        (m) => m.AdministrationPageModule
      ),
    canActivate: [AuthGuard, adminGuard],
    data: {
      authGuardPipe: redirectUnauthorizedToLogin,
    }
  },
  {
    path: 'billing',
    loadChildren: () =>
      import('./pages/modules/billing/billing.module').then(
        (m) => m.BillingPageModule
      ),
    canActivate: [AuthGuard],
    data: {
      authGuardPipe: redirectUnauthorizedToLogin
    }
  },
  {
    path: 'inventory',
    loadChildren: () =>
      import('./pages/modules/inventory/inventory.module').then(
        (m) => m.InventoryPageModule
      ),
    canActivate: [AuthGuard],
    data: {
      authGuardPipe: redirectUnauthorizedToLogin
    }
  },
  {
    path: 'reporting',
    loadChildren: () =>
      import('./pages/modules/reporting/reporting.module').then(
        (m) => m.ReportingPageModule
      ),
    canActivate: [AuthGuard, adminGuard],
    data: {
      authGuardPipe: redirectUnauthorizedToLogin
    }
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
