import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { Auth, IdTokenResult } from '@angular/fire/auth';
import { ToastController } from '@ionic/angular';

/**Guard than validate if User has the role "admin" to access routes asigned
 * to this one. This is the higher access role, admin User can get access
 * to all routes.
 *
 * @returns true if has the role, otherwise returns false
 */
export const adminGuard: CanActivateFn = async () => {
  const auth = inject(Auth);
  const router = inject(Router);
  const toastController = inject(ToastController);
  await auth.authStateReady();

  let tokenRusult: IdTokenResult = await auth.currentUser?.getIdTokenResult() as IdTokenResult;
  let roles: string[] = tokenRusult.claims['roles'] as string[]

  if (auth.currentUser && roles.includes('admin')) return true;

  router.navigate(['home']);
  await presentUnauthorizedToast(toastController, "Lo sentimos, pero esta ruta está protegida para el administrador");
  return false;
};

/**Guard than validate if User has the role "seller" to access routes asigned
 * to this one.
 *
 * @returns true if has the role, otherwise returns false
 */
export const sellerGuard: CanActivateFn = async () => {
  const auth = inject(Auth);
  const router = inject(Router);
  await auth.authStateReady();

  let tokenRusult: IdTokenResult = await auth.currentUser?.getIdTokenResult() as IdTokenResult;
  let roles: string[] = tokenRusult.claims['roles'] as string[]

  if (auth.currentUser && roles.includes('seller')) return true;

  router.navigate(['home']);
  return false;
};


export async function presentUnauthorizedToast(ctrl: ToastController, message: string) {
  const toastCtrl = ctrl;

  const toast = await toastCtrl.create({
    animated: true,
    message,
    duration: 7000,
    icon: 'ban-outline',
    color: 'danger',
    id: 'unauthorized-toast',
    header: 'No tiene permisos',
    position: 'top',
    buttons: [
      {
        text: 'Aceptar',
        side: 'end'
      }
    ]
  })

  await toast.present();

}
