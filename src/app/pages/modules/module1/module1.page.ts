import { Component, OnInit } from '@angular/core';
import MenubarListItemModel from 'src/app/shared/components/menubar/util/MenubarListItem.model';
// import { Printer } from '@awesome-cordova-plugins/printer'

@Component({
  selector: 'p-module1',
  templateUrl: './module1.page.html',
  styleUrls: ['./module1.page.scss'],
})
export class Module1Page implements OnInit {
  moduleName: string = 'Módulo 1 (Ejemplo)';
  menu: MenubarListItemModel[] = [
    {
      icon: 'baseball',
      title: 'Opción de Baseball',
      description: '',
      route: '',
    },
    {
      icon: 'basketball',
      title: 'Opción de Basketball',
      description: '',
      route: '',
    },
    {
      icon: 'football',
      title: 'Opción de Football',
      description: '',
      route: 'module1',
    },
    {
      icon: 'american-football',
      title: 'Opción de American Football',
      description: '',
      route: 'account',
    },
  ];

  constructor() { }

  ngOnInit() {

    // const doc = document.createElement('div');
    // const text = document.createElement('h4')
    // text.innerText = 'Hola mundo';
    // doc.appendChild(text)

    // const windPrinter = window.print();
    // windPrinter?.document.write('Hola mundo');
    // windPrinter?.document.close();
    // const pl = windPrinter?.cordova.plugins as CordovaPlugins
    // windPrinter?.print();
    // windPrinter?.close();

  }
}
