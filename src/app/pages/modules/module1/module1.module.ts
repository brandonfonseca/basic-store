import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Module1PageRoutingModule } from './module1-routing.module';

import { Module1Page } from './module1.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Module1PageRoutingModule,
    SharedModule
  ],
  declarations: [Module1Page]
})
export class Module1PageModule {}
