import { Component, OnInit } from '@angular/core';
import MenubarListItemModel from 'src/app/shared/components/menubar/util/MenubarListItem.model';

@Component({
  selector: 'p-administration',
  templateUrl: './administration.page.html',
  styleUrls: ['./administration.page.scss'],
})
export class AdministrationPage implements OnInit {
  moduleName: string = 'Administración';
  menu: MenubarListItemModel[] = [
    {
      icon: 'people',
      title: 'Gestionar usuarios',
      description: '',
      route: 'administration/users-management',
    },
  ];

  constructor() {}

  ngOnInit() {}
}
