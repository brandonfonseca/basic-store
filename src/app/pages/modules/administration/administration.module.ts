import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdministrationPageRoutingModule } from './administration-routing.module';

import { AdministrationPage } from './administration.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserCreationComponent } from './children-page/user-creation/user-creation.component';
import { UserManagementComponent } from './children-page/user-management/user-management.component';
import { UserDetailComponent } from './children-page/user-detail/user-detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AdministrationPageRoutingModule,
    SharedModule],
  declarations: [
    AdministrationPage,
    UserCreationComponent,
    UserManagementComponent,
    UserDetailComponent,
  ],
})
export class AdministrationPageModule { }
