export interface UsersModel {
  id: string;
  photoURL: string;
  names: string;
  lastNames: string;
  email: string;
  phoneNumber: string;
  identification: string;
  address: string;
  roles: string[];
}

export interface UsersModelDTO extends UsersModel, Omit<string, 'id'> {
  password: string;
}
