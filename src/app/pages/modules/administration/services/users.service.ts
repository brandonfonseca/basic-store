import { Injectable } from '@angular/core';
import { UsersModel, UsersModelDTO } from './users.type';
import { Auth, User } from '@angular/fire/auth';
import {
  doc,
  setDoc,
  deleteDoc,
  getDoc,
  updateDoc,
} from '@angular/fire/firestore';
import { getFirestore } from '@firebase/firestore';
import { getFunctions, httpsCallableFromURL } from '@angular/fire/functions';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  db = getFirestore(this.auth.app);

  constructor(private auth: Auth) { }

  async createUser(newUser: UsersModelDTO) {
    const {
      id,
      names,
      lastNames,
      email,
      photoURL,
      phoneNumber,
      password,
      roles,
      ...userDetail
    } = newUser;

    const functions = getFunctions();
    const createUserFunction = httpsCallableFromURL<any, User>(
      functions,
      // 'https://createuser-7x6fit2hza-uc.a.run.app'
      environment.functions.users.createUser
    );

    //Creating Auth User
    const response = await createUserFunction({
      displayName: `${names} ${lastNames}`,
      email,
      password,
      phoneNumber,
      photoURL,
      roles
    });

    const { uid } = response.data;

    //Creating record on 'Users' collection excluding 'password field'
    return setDoc(doc(this.db, 'users', uid), {
      names,
      email,
      lastNames,
      phoneNumber,
      photoURL,
      roles,
      ...userDetail,
    });
  }

  async modifiedUser(newUserData: UsersModel) {
    const {
      id,
      names,
      lastNames,
      email,
      photoURL,
      phoneNumber,
      roles,
      ...userDetail
    } = newUserData;

    const functions = getFunctions();
    const updateUserFunction = httpsCallableFromURL<any, User>(
      functions,
      // 'https://updateuser-7x6fit2hza-uc.a.run.app'
      environment.functions.users.updateUser
    );

    //Creating Auth User

    await updateUserFunction({
      uid: id,
      displayName: `${names} ${lastNames}`,
      email,
      phoneNumber,
      photoURL,
      roles
    });

    const docRef = doc(this.db, 'users', id);

    return updateDoc(docRef, {
      names,
      lastNames,
      email,
      photoURL,
      phoneNumber,
      roles,
      ...userDetail,
    });
  }

  async readAllUsers() {
    const functions = getFunctions();
    const readAllUsersFunction = httpsCallableFromURL<any, User[]>(
      functions,
      environment.functions.users.readAllUsers
    );

    const usersAuth = await readAllUsersFunction();

    return usersAuth.data;
  }

  readUserWithDetailById(id: string) {
    return getDoc(doc(this.db, 'users', id));
  }

  async delete(user: UsersModel) {
    const { id } = user;

    console.log(id);

    const functions = getFunctions();
    const deleteUserFunction = httpsCallableFromURL<{ uid: string }, any>(
      functions,
      environment.functions.users.deleteUser
    );

    await deleteUserFunction({
      uid: id,
    });

    return deleteDoc(doc(this.db, 'users', id));
  }
}
