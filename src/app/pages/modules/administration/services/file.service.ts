import { Injectable } from '@angular/core';
import {
  Storage,
  getDownloadURL,
  ref,
  uploadBytes,
  deleteObject,
} from '@angular/fire/storage';

@Injectable({
  providedIn: 'root',
})
export class FileService {
  constructor(private storage: Storage) {}

  async uploadImage(imgName: string, file: Blob) {
    const imgRef = ref(this.storage, imgName);
    return await uploadBytes(imgRef, file);
  }

  readImage(imgName: string) {
    const imgRef = ref(this.storage, imgName);
    return getDownloadURL(imgRef);
  }

  deleteImage(imgName: string) {
    const imgRef = ref(this.storage, imgName);
    return deleteObject(imgRef);
  }
}
