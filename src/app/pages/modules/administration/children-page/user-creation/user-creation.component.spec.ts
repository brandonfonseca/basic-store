import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserCreationComponent } from './user-creation.component';
import { FileService } from '../../services/file.service';
import { UsersService } from '../../services/users.service';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { getStorage, provideStorage } from '@angular/fire/storage';

describe('Caso de prueba A06', () => {
  let component: UserCreationComponent;
  let fixture: ComponentFixture<UserCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserCreationComponent],
      providers: [UsersService, FileService],
      imports: [
        ReactiveFormsModule,
        RouterModule.forRoot([]),
        IonicModule.forRoot(),
        provideFirebaseApp(() =>
          initializeApp({
            apiKey: 'AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs',
            authDomain: 'bs-bif.firebaseapp.com',
            projectId: 'bs-bif',
            storageBucket: 'bs-bif.appspot.com',
            messagingSenderId: '389025844621',
            appId: '1:389025844621:web:0e69097f5c33e9ea9381c4',
          })
        ),
        provideAuth(() => getAuth()),
        provideStorage(() => getStorage()),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Se debe visualizar correctamente el formulario para la creación de usuarios', () => {
    let element = fixture.nativeElement.querySelectorAll('form');

    expect(element.length).toBe(1);
  });
});
