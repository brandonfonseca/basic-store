import { UsersModelDTO } from './../../services/users.type';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { FileService } from '../../services/file.service';
import { FirebaseError } from '@angular/fire/app';
import { validateFormControlErrors } from 'src/app/shared/util/FormsValidations';
import { utilConstants } from 'src/util/util.constants';

@Component({
  selector: 'cp-user-creation',
  templateUrl: './user-creation.component.html',
  styleUrls: ['./user-creation.component.scss'],
})
export class UserCreationComponent implements OnInit {
  userCreationFormGroup = new FormGroup({
    photoURL: new FormControl<string>(''),
    names: new FormControl('', [Validators.required]),
    lastNames: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.pattern(utilConstants.regexPatterns.password),
    ]),
    phoneNumber: new FormControl('', [Validators.maxLength(8), Validators.minLength(8)]),
    identification: new FormControl('', [
      Validators.required,
      //TODO: Validate february and the other months with 31 and 30 days per month
      Validators.pattern(utilConstants.regexPatterns.dni),
    ]),
    address: new FormControl(''),
    roles: new FormControl<string[]>({ value: [], disabled: false }, [
      Validators.required,
      Validators.minLength(1),
    ]),
  });

  pictureSelected: File | null = null;

  picturePreview: string = '';
  noPhoto: string = utilConstants.noPhoto;

  isLoadingPicture: boolean = false;
  isCheckingCreation: boolean = false; // "..." spinner feedback

  constructor(
    private router: Router,
    private userService: UsersService,
    private fileService: FileService
  ) {}

  /*TODO: Create an Alert Message preventing this view to be dismissed
  whenn click on "VOLVER" button if form is dirty...
  */
  ngOnInit() {}

  openFileChooser(input: HTMLInputElement) {
    console.log('abriendo...');
    input.click();
  }

  /** Upload an image to firebase storage when an event is dispatched
   *
   * @param event
   */
  uploadImageToPreview(event: Event) {
    const fileEvent = event.target as HTMLInputElement;
    const files = fileEvent.files as FileList;

    if (files.length == 0) {
      this.userCreationFormGroup.setErrors({
        noFileSelectedError: true,
      });

      return;
    } else if (!files[0].type.includes('image/')) {
      this.userCreationFormGroup.setErrors({
        fileTypeNotAllowed: true,
      });

      return;
    }

    const instant = Date.now();

    this.isLoadingPicture = true; //when image start to upload, init progress indicator

    this.fileService
      .uploadImage(`perfiles/profile${instant}`, files[0])
      .then(async (response) => {
        const downloadURL = await this.fileService.readImage(
          response.metadata.fullPath
        );

        this.picturePreview = downloadURL;
        this.isLoadingPicture = false;
      });
  }

  createNewUser() {
    const user: UsersModelDTO = this.userCreationFormGroup
      .value as UsersModelDTO;

    user.photoURL = this.picturePreview || this.noPhoto;
    this.isCheckingCreation = true;

    this.userService
      .createUser(user)
      .then((response) => {
        console.log(
          'Se creó con éxito el usuario:',
          user.names,
          user.lastNames
        );
        this.router.navigate(['administration', 'users-management']);
        this.isCheckingCreation = false;
      })
      .catch((error: FirebaseError) => {
        console.error(error);
        this.isCheckingCreation = false;
      });
  }

  goBack() {
    this.router.navigate(['administration', 'users-management']);
  }

  discardUser() {
    this.userCreationFormGroup.reset();
    this.picturePreview = '';
  }

  validateInputErrors(formControlName: string): string {
    const validations = {
      required: 'El valor de este campo es requerido',
      email: 'Porfavor, ingrese un email válido ej: algo@algo.com',
      minlength: `El mínimo de caracteres permitidos es de 8 caracteres`,
      maxLength: `El máximo de caracteres permitidos es 8`,
      pattern:
        'El valor debe incluir: mayúsculas, minúsculas, números y símbolos ej: Basic2024$, *2024Basic',
      invalidCredentials: 'Las credenciales ingresadas son invalidas',
    };

    return validateFormControlErrors(
      formControlName,
      this.userCreationFormGroup,
      validations
    );
  }
}
