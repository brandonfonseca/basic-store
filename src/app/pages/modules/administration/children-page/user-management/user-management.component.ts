import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { User } from '@angular/fire/auth';

@Component({
  selector: 'cp-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss'],
})
export class UserManagementComponent implements OnInit {
  users: User[] = [];
  noPhoto: string =
    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png';

  constructor(private router: Router, private userService: UsersService) {}

  ngOnInit() {
    this.userService.readAllUsers().then((data) => {
      // data.data.forEach((doc) => {
      // });
      const rawUsers = data;

      rawUsers.forEach((doc) => {
        const user = doc;
        this.users.push(user);
      });

      this.users = data;
    });
  }

  viewUserDetail(userId: string) {
    this.router.navigate(['administration', 'users-detail', userId]);
  }

  goToCreationUserView() {
    this.router.navigate(['administration', 'users-creation']);
  }
}
