import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdministrationPage } from './administration.page';
import { UserCreationComponent } from './children-page/user-creation/user-creation.component';
import { UserDetailComponent } from './children-page/user-detail/user-detail.component';
import { UserManagementComponent } from './children-page/user-management/user-management.component';
import { WelcomeComponent } from 'src/app/shared/components/welcome/welcome.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrationPage,
    children: [
      {
        path: 'welcome',
        component: WelcomeComponent,
        data: {
          module: 'administration',
        }
      },
      {
        path: 'users-management',
        component: UserManagementComponent,
      },
      {
        path: 'users-detail/:userId',
        component: UserDetailComponent,
      },
      {
        path: 'users-creation',
        component: UserCreationComponent,
      },
      {
        path: '**',
        redirectTo: 'welcome',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrationPageRoutingModule { }
