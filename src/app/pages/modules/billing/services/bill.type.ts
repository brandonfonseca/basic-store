import { Timestamp } from '@angular/fire/firestore';

export interface BillModel {
  id: string;
  billNumber: string;
  issueDate: Timestamp;
  subtotal: number;
  total: number;
  discount: number;
  sellerName: string;
  sellerEmail: string;
  items: BillDetailModel[];
}

export interface BillDetailModel {
  itemName: string;
  itemCode: string;
  itemPrice: number;
  itemDiscount: number;
  itemQuantity: number;
  itemAmount: number;
}

export interface BillModelDTO extends BillModel, Omit<string,'id'>{}
