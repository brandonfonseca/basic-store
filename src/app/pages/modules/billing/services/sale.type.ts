import { Timestamp } from '@angular/fire/firestore';
import { BillModel } from './bill.type';

export interface SaleModel {
  id: string;
  issueDate: Timestamp;
  total: number;
  sellerName: string;
  bill: BillModel;
  isTyped: boolean; //This is for Typed Sales, just for reporting utilities
}

export interface SaleModelDTO extends SaleModel, Omit<string, 'id'> {}
