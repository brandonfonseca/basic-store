import { Injectable } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { BillDetailModel, BillModel } from './bill.type';
import {
  getFirestore,
  getDocs,
  collection,
  getDoc,
  addDoc,
  updateDoc,
  deleteDoc,
  query,
  where,
  and,
  increment,
  setDoc,
  doc,
} from '@angular/fire/firestore';
import { ProductModel } from '../../inventory/services/products.type';

@Injectable({
  providedIn: 'root',
})
export class BillingService {
  db = getFirestore(this.auth.app);

  constructor(private auth: Auth) { }

  async readAllBills() {
    const bills = (await getDocs(collection(this.db, 'bills'))).docs;

    return bills;
  }

  readBillById(id: string) {
    return getDoc(doc(this.db, 'bills', id));
  }

  async createBill(newBill: BillModel) {

    await this.incrementCounter('bill');//Incrementing value bill number
    const number = (await getDoc(doc(this.db, 'counters', 'bill'))).data() as { count: number };
    newBill.billNumber = `fact-${number.count}`;

    const bill = await addDoc(collection(this.db, 'bills'), newBill);
    await this.reduceProducts(newBill.items);

    const createdBill = (await getDoc(bill)).data() as BillModel;

    const { issueDate, total, sellerName } = createdBill;

    const sale = {
      issueDate,
      total,
      sellerName,
      bill: createdBill,
      isTyped: false,
    };

    return addDoc(collection(this.db, 'sales'), sale);
  }

  /**Increment a value for bills or product code,
   * this one increment value for collection "counters"
   * one by one depending of counterName parameter value
   *
   * @param counterName
   * @returns Promise
   */
  incrementCounter(counterName: 'bill' | 'productCode') {

    return setDoc(doc(this.db, 'counters', counterName), {
      'count': increment(1)
    },{merge: true})
  }

  async reduceProducts(reduces: BillDetailModel[]) {

    const docs = await getDocs(
      query(
        collection(this.db, 'products'),
        where(
          'code',
          'in',
          reduces.map((r) => r.itemCode)
        )
      )
    );

    //Mapping docs to update one by one
    docs.forEach(d => {

      const product = d.data() as ProductModel; //Product found
      const itemsToDecrement = reduces.find(item => item.itemCode = product.code); //Product Filtered
      const decrement = itemsToDecrement?.itemQuantity as number * -1;

      updateDoc(doc(this.db, 'products', d.id), {
        'detail.stock': increment(decrement)
      })
    })
  }

  updateBill(changedBill: BillModel) {
    return updateDoc(doc(this.db, 'bills', changedBill.id), {
      ...changedBill,
    });
  }

  deleteBill(id: string) {
    return deleteDoc(doc(this.db, 'bills', id));
  }

  // HANDLE PRODUCTS

  findProductsByCodeOrName(codeOrName: string) {
    return getDocs(
      query(
        collection(this.db, 'products'),
        and(where('code', '==', codeOrName))
        // orderBy('name', 'desc'),
        // startAt(codeOrName)
      )
    );
  }
}
