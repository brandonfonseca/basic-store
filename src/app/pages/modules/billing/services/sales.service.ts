import { Injectable } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import {
  getFirestore,
  getDocs,
  collection,
  getDoc,
  doc,
  addDoc,
  updateDoc,
  deleteDoc,
  getAggregateFromServer,
  query,
  sum,
  where,
  Timestamp,
  and,
  orderBy,
} from '@angular/fire/firestore';
import { SaleModel, SaleModelDTO } from './sale.type';

@Injectable({
  providedIn: 'root',
})
export class SalesService {
  db = getFirestore(this.auth.app);

  constructor(private auth: Auth) { }

  async readAllSales() {
    const sales = (await getDocs(collection(this.db, 'sales'))).docs;

    return sales;
  }

  async readAllSalesByPagination(elementPerPage: number, period: 'today' | 'week' | 'month' | 'always' = 'today') {
    const from = this.#calculateDate(period)

    const q = query(
      collection(this.db, 'sales',),
      where('issueDate', '>=', from),
      orderBy('issueDate', 'desc')
    )

    return (await getDocs(q)).docs;
  }

  readSaleById(id: string) {
    return getDoc(doc(this.db, 'sales', id));
  }

  /**Obtain a short general information about sales
   * state.
   *
   * @param sellerEmail current user seller
   * @returns a summary of sales of current day,
   *  current month and current user
   */
  async readSaleSummary(sellerEmail: string) {
    const today = Timestamp.fromMillis(Date.parse(
      new Date().toDateString()
    ));

    const total = await getAggregateFromServer(
      query(collection(this.db, 'sales'),
        where('issueDate', '>=', this.#calculateDate('month'))), {
      amount: sum('total')
    })

    const currentSellerData = await getAggregateFromServer(
      query(collection(this.db, 'sales'),
        where('bill.sellerEmail', '==', sellerEmail),
        where('issueDate', '>=', today)),
      {
        amount: sum('total')
      })

    const todaySales = await getAggregateFromServer(
      query(collection(this.db, 'sales'), where('issueDate', '>=', today)), {
      amount: sum('total')
    })

    return {
      monthSales: total.data().amount,
      todaySales: todaySales.data().amount,
      userSales: currentSellerData.data().amount
    }
  }

  createSale(newSale: SaleModelDTO) {
    return addDoc(collection(this.db, 'sales'), newSale);
  }

  updateSale(changedSale: SaleModel) {
    return updateDoc(doc(this.db, 'sales', changedSale.id), {
      ...changedSale,
    });
  }

  deleteSale(id: string) {
    return deleteDoc(doc(this.db, 'sales', id));
  }

  /**Calculate a date starting from four periods
   *
   * @param period a period of time to start
   * @returns a start date from period selected
   */
  #calculateDate(period: 'today' | 'week' | 'month' | 'always' = 'today') {

    const date = new Date();

    //Date for today
    const dateFrom = {
      day: date.getDate(),
      month: date.getMonth(),
      year: date.getFullYear()
    }

    if (period === 'week') dateFrom.day -= date.getDay(); // First day of the week
    else if (period === 'month') dateFrom.day = 1; // first day of month
    else if (period === 'always') dateFrom.year = 1970; //From always

    const newDate = new Date();
    newDate.setDate(dateFrom.day);
    newDate.setMonth(dateFrom.month);
    newDate.setFullYear(dateFrom.year);

    const from = Timestamp
      .fromMillis(
        Date.parse(
          newDate.toDateString()
        )
      );

    return from;
  }
}
