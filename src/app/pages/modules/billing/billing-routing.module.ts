import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BillingPage } from './billing.page';
import { PointOfSaleComponent } from './children-pages/point-of-sale/point-of-sale.component';
import { SalesComponent } from './children-pages/sales/sales.component';
import { SaleCreationComponent } from './children-pages/sale-creation/sale-creation.component';
import { SaleDetailComponent } from './children-pages/sale-detail/sale-detail.component';
import { WelcomeComponent } from 'src/app/shared/components/welcome/welcome.component';


const routes: Routes = [
  {
    path: '',
    component: BillingPage,
    children: [
      {
        path: 'welcome',
        component: WelcomeComponent,
        data: {
          module: 'billing',
        }
      },
      {
        path: 'pos',
        component: PointOfSaleComponent,
      },
      {
        path: 'sales-creation',
        component: SaleCreationComponent,
      },
      {
        path: 'sales-detail/:saleId',
        component: SaleDetailComponent,
      },
      {
        path: 'sales',
        component: SalesComponent,
      },
      {
        path: '**',
        redirectTo: 'welcome',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BillingPageRoutingModule { }
