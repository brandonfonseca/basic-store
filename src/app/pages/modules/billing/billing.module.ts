import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BillingPageRoutingModule } from './billing-routing.module';

import { BillingPage } from './billing.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { SalesComponent } from './children-pages/sales/sales.component';
import { PointOfSaleComponent } from './children-pages/point-of-sale/point-of-sale.component';
import { SaleCreationComponent } from './children-pages/sale-creation/sale-creation.component';
import { SaleDetailComponent } from './children-pages/sale-detail/sale-detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    BillingPageRoutingModule,
  ],
  declarations: [
    BillingPage,
    SalesComponent,
    SaleCreationComponent,
    SaleDetailComponent,
    PointOfSaleComponent,
  ],
})
export class BillingPageModule { }
