import { Component, OnInit } from '@angular/core';
import MenubarListItemModel from 'src/app/shared/components/menubar/util/MenubarListItem.model';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.page.html',
  styleUrls: ['./billing.page.scss'],
})
export class BillingPage implements OnInit {
  moduleName: string = 'Facturación';
  menu: MenubarListItemModel[] = [
    {
      icon: 'storefront',
      title: 'Punto de venta',
      description: '',
      route: 'billing/pos',
    },
    {
      icon: 'logo-usd',
      title: 'Registro de ventas',
      description: '',
      route: 'billing/sales',
    },
  ];

  constructor() {}

  ngOnInit() {}
}
