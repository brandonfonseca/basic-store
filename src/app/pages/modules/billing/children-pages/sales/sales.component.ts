import { Component, OnInit } from '@angular/core';
import { SalesService } from '../../services/sales.service';
import { SaleModel } from '../../services/sale.type';
import { Router } from '@angular/router';
import { utilConstants } from 'src/util/util.constants';
import { Auth, User } from '@angular/fire/auth';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'c-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss'],
})
export class SalesComponent implements OnInit {
  sales: SaleModel[] = [];
  noPhoto: string = utilConstants.noProductPhoto;
  salesSummary = {
    monthSales: 0,
    todaySales: 0,
    userSales: 0
  }

  filterDateControl = new FormControl<'today' | 'week' | 'month' | 'always'>('today');

  currentUser: User | null = null;

  isLoading: boolean = true;

  constructor(private router: Router, private salesService: SalesService, private auth: Auth) { }

  async ngOnInit() {

    await this.auth.authStateReady();
    this.currentUser = this.auth.currentUser;

    await this.onFilterDateChange();

    this.salesSummary = await this.salesService.readSaleSummary(this.currentUser?.email || '');
  }

  viewProductDetail(productId: string) {
    this.router.navigate(['billing', 'sales-detail', productId]);
  }

  goToCreationProductView() {
    this.router.navigate(['billing', 'sales-creation']);
  }

  today() {
    return new Date().toLocaleDateString('es-ES', {
      dateStyle: 'full'
    })
  }

  async onFilterDateChange() {

    const period = this.filterDateControl.value || 'today';
    this.sales = [];
    this.isLoading = true;

    this.salesService.readAllSalesByPagination(6, period).then((data) => {
      data.forEach((doc) => {
        const product = doc.data() as SaleModel;
        product.id = doc.id;
        this.sales.push(product);
      });

      this.isLoading = false;
    });
  }

}
