import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs';
import { BillingService } from '../../services/billing.service';
import { utilConstants } from 'src/util/util.constants';
import { SalesService } from '../../services/sales.service';
import { SaleModel } from '../../services/sale.type';
import { FormControl } from '@angular/forms';
import { BillDetailModel } from '../../services/bill.type';

@Component({
  selector: 'c-sale-detail',
  templateUrl: './sale-detail.component.html',
  styleUrls: ['./sale-detail.component.scss'],
})
export class SaleDetailComponent implements OnInit {

  saleSelected: SaleModel | null = null;
  noProductPhoto = utilConstants.noProductPhoto;
  elementsPerPage = new FormControl(6);
  elementsToShow: BillDetailModel[] = [];
  currentPage = 1;
  pages = 1;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private saleService: SalesService
  ) { }

  ngOnInit() {
    this.getSaleDetail();
  }

  getSaleDetail() {
    this.activeRoute.params
      .pipe(map<Params, string>((params) => params['saleId']))
      .subscribe(async (id) => {
        const data = await this.saleService.readSaleById(id);

        this.saleSelected = data.data() as SaleModel;
        this.saleSelected.id = data.id;

        this.loadElementsPerPage();

      });
  }

  goBack() {
    this.router.navigate(['billing', 'sales'])
  }

  deleteBill() {
    console.log("Eliminando la factura: ", this.saleSelected?.bill.billNumber);
  }

  paginate(direction: 'back' | 'next') {

    if (direction === 'next') {
      this.currentPage++;
    } else {
      this.currentPage--;
    }

    this.loadElementsPerPage();

  }

  loadElementsPerPage() {
    const elements = this.elementsPerPage.value || 0;
    const rawPages = (this.saleSelected?.bill.items.length || 1) / elements
    this.pages = Math.round(rawPages)


    this.elementsToShow = this.saleSelected?.bill.items.slice(
      (elements * this.currentPage - elements),
      elements * this.currentPage
    ) as BillDetailModel[]


    if (rawPages < this.pages) this.pages += 1;
    else if (rawPages > this.pages) this.pages += 1;
  }

}
