import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ToastController } from '@ionic/angular';

import { PointOfSaleComponent } from './point-of-sale.component';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductsManagementComponent } from '../../../inventory/children-pages/products-management/products-management.component';
import { BillingService } from '../../services/billing.service';

describe('Caso de prueba A02', () => {
  let component: PointOfSaleComponent;
  let fixture: ComponentFixture<PointOfSaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PointOfSaleComponent],
      providers: [BillingService, ToastController],
      imports: [
        ReactiveFormsModule,
        IonicModule.forRoot(),
        provideFirebaseApp(() =>
          initializeApp({
            apiKey: 'AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs',
            authDomain: 'bs-bif.firebaseapp.com',
            projectId: 'bs-bif',
            storageBucket: 'bs-bif.appspot.com',
            messagingSenderId: '389025844621',
            appId: '1:389025844621:web:0e69097f5c33e9ea9381c4',
          })
        ),
        provideAuth(() => getAuth()),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointOfSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Se debe poder visualización de componente de punto de ventas', () => {
    expect(component).toBeTruthy();
  });

});
