import { Component, OnInit } from '@angular/core';
import { utilConstants } from 'src/util/util.constants';
import {
  ProductModel,
  ProductOnInvoiceModel,
} from '../../../inventory/services/products.type';
import { FormControl, FormGroup } from '@angular/forms';
import { BillingService } from '../../services/billing.service';
import { Auth, User } from '@angular/fire/auth';
import { Timestamp } from '@angular/fire/firestore';
import { BillDetailModel, BillModel } from '../../services/bill.type';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'c-point-of-sale',
  templateUrl: './point-of-sale.component.html',
  styleUrls: ['./point-of-sale.component.scss'],
})
export class PointOfSaleComponent implements OnInit {
  pickerColumns = [
    {
      name: 'payment',
      options: [
        {
          text: 'Efectivo (Predeterminado)',
          value: 'cash',
        },
        {
          text: 'Tarjeta (Crédito/Débito)',
          value: 'card',
        },
        {
          text: 'Transferencia',
          value: 'transaction',
        },
        {
          text: 'Crédito',
          value: 'credit',
        },
      ],
    },
  ];

  pickerButtons = [
    {
      text: 'Cancelar',
      role: 'cancel',
    },
    {
      text: 'Confirmar',
      handler: (value: any) => {
        console.log(value);

        console.log(`You selected: ${value.payment.value}`);
      },
    },
  ];

  searchBarControl = new FormControl('');

  // Attribute used for bill summary
  billFormGroup = new FormGroup({
    issueDate: new FormControl<Timestamp | null>(null),
    sellerName: new FormControl(''),
    sellerEmail: new FormControl(''),
    subtotal: new FormControl<number>(0),
    total: new FormControl<number>(0.0),
    discount: new FormControl<number>(0.0),
    items: new FormControl<BillDetailModel[]>([]),
  });

  products: ProductOnInvoiceModel[] = []; //Product list for billing

  productsFound: ProductModel[] = []; //List for searchbar

  noPhoto = utilConstants.noProductPhoto;
  popoverIsVisible = false;
  isCheckingOut = false;

  currentUser: User | null = null;
  timerId: any;
  enterKeyPressed = false;

  constructor(
    private billingServices: BillingService,
    private auth: Auth,
    private toastCtrl: ToastController
  ) { }

  async ngOnInit() {
    await this.auth.authStateReady();
    this.currentUser = this.auth.currentUser;

    //Asigning values to form
    this.billFormGroup.patchValue({
      sellerName: this.currentUser?.displayName,
      sellerEmail: this.currentUser?.email,
    });
  }

  closePopover() {
    this.productsFound = [];
    this.popoverIsVisible = false;
  }

  showPopover() {
    this.popoverIsVisible = true;
  }

  today() {
    return new Date().toLocaleDateString('es-ES', {
      dateStyle: 'full'
    })
  }

  addProductByTap(product: ProductModel) {

    if (!product) {
      return;
    }

    if ((product.detail.stock as number) < 1) {
      alert('el producto no tiene existencias');
      return;
    }

    let productToFind: ProductOnInvoiceModel | undefined = this.products.find(
      (p, i) => product.code === p.code
    );

    if (productToFind) {
      // If product is found, populate values like "quantity" and "totalValue"
      productToFind.quantity++;
      productToFind.totalValue =
        productToFind.quantity * (product.detail?.salePrice as number);
    } else {
      //If product not exist on list, just push it
      productToFind = {
        ...product,
        quantity: 1,
        totalValue: product.detail?.salePrice as number,
      };
      this.products.unshift(productToFind);
    }

    this.incrementBill(parseFloat(productToFind.detail.salePrice.toString()));

    this.closePopover();
    this.searchBarControl.reset();
  }

  async addProductByKey(event: KeyboardEvent) {
    if (event.key.toLocaleLowerCase() !== 'enter') return;


    if (this.productsFound.length < 1) {
      await this.findProducts();
    }

    this.addProductByTap(this.productsFound[0]);
    this.productsFound = [];

    this.enterKeyPressed = true; //To indicate than enter key has been pressed
  }

  incrementBill(value: number) {
    const { subtotal, total, discount } = this.billFormGroup.controls;

    subtotal.setValue((subtotal.value as number) + value);
    total.setValue((subtotal.value as number) - (discount.value as number));
  }

  decrementBill(value: number) {
    const { subtotal, total, discount } = this.billFormGroup.controls;

    subtotal.setValue((subtotal.value as number) - value);
    total.setValue((subtotal.value as number) - (discount.value as number));
  }

  removeProduct(index: number) {
    this.products = this.products.filter((product, i) => {
      if (i === index) {
        this.decrementBill(product.totalValue);
        return false;
      }
      return true;
    });

    if (this.products.length < 1) this.onDiscard();
  }

  onDiscount() {
    const { subtotal, total, discount } = this.billFormGroup.controls;
    total.setValue((subtotal.value as number) - (discount.value as number));
  }

  onChangeQuantity(index: number, event: CustomEvent) {
    console.log('dispatching..');

    let productFound: ProductOnInvoiceModel | undefined = this.products.find(
      (p, i) => i === index
    ) as ProductOnInvoiceModel;
    const increment = event.detail.value as number;

    const subtotalc = this.billFormGroup.get('subtotal');
    const discountc = this.billFormGroup.get('discount');
    const totalc = this.billFormGroup.get('total');
    let subtotalv: number = subtotalc?.value as number;

    subtotalv -= parseFloat(productFound.totalValue.toString());

    // // Calculating new product total value in bill
    productFound.quantity = increment;
    productFound.totalValue =
      productFound.quantity * (productFound.detail.salePrice as number);

    subtotalc?.setValue(
      subtotalv + parseFloat(productFound.totalValue.toString())
    );
    totalc?.setValue(
      (subtotalc?.value as number) - (discountc?.value as number)
    );
  }

  onCheckout() {
    if (this.products.length < 1) return;

    this.isCheckingOut = true;

    const newBill = this.billFormGroup.value as BillModel;

    const itemsOnBill = this.products.map((p) => {
      const billDetail: BillDetailModel = {
        itemName: p.name,
        itemCode: p.code,
        itemPrice: p.detail.salePrice,
        itemDiscount: 0,
        itemQuantity: p.quantity,
        itemAmount: p.totalValue,
      };
      return billDetail;
    });

    newBill.items = itemsOnBill;
    newBill.issueDate = Timestamp.now();

    this.billingServices
      .createBill(newBill)
      .then(async (result) => {
        this.onDiscard();
        console.log(result);
        await this.showUserFeedback();
      })
      .catch((error) => {
        console.log(error);
      }).finally(() => this.isCheckingOut = false); //Disabling loader
  }

  onDiscard() {
    this.products = [];
    this.productsFound = [];
    this.billFormGroup.patchValue({
      subtotal: 0.0,
      discount: 0.0,
      total: 0,
    });
    this.searchBarControl.reset();
    console.log('Discarding...');
  }

  async findProducts() {
    console.log('Buscando productos...');

    const code = this.searchBarControl?.value as string;
    const values = await this.billingServices.findProductsByCodeOrName(code);

    this.productsFound = values.docs.map((doc) => {
      const product: ProductModel = doc.data() as ProductModel;
      product.id = doc.id;
      return product;
    });
  }

  async onType() {
    const searchValue = this.searchBarControl.value?.trim() as string;

    if (searchValue.length === 0) {
      this.closePopover();
    } else {
      this.#customDebounce(async () => {
        await this.findProducts();
        this.showPopover();
      })
    }
  }

  async showUserFeedback() {

    const toast = await this.toastCtrl.create(
      {
        icon: 'checkmark-circle',
        header: 'Éxito',
        message: 'Venta realizada con éxito',
        color: 'success',
        duration: 2000,
        position: 'top',
        buttons: [
          {
            text: 'Aceptar',
            role: 'cancel'
          }
        ]
      }
    )

    toast.present();

  }

  #customDebounce(callback: () => any, time = 500) {

    if (this.timerId) clearTimeout(this.timerId);

    this.timerId = setTimeout(() => {
      if (this.enterKeyPressed)
        return;
      else {
        callback(); //Execute the function
      }
    }, time);

    this.enterKeyPressed = false; // Reset State
  }

}
