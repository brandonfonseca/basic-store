import { SaleModelDTO } from './../../services/sale.type';
import { Component, OnInit } from '@angular/core';
import { SalesService } from '../../services/sales.service';
import { FirebaseError } from '@angular/fire/app';
import { FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { validateFormControlErrors } from 'src/app/shared/util/FormsValidations';
import { Timestamp } from '@angular/fire/firestore';
import { Auth } from '@angular/fire/auth';

@Component({
  selector: 'c-sale-creation',
  templateUrl: './sale-creation.component.html',
  styleUrls: ['./sale-creation.component.scss'],
})
export class SaleCreationComponent implements OnInit {
  saleCreationFormGroup = this.fb.group({
    issueDate: this.fb.control<Timestamp>(Timestamp.now()),
    sellerName: this.fb.control<string>(''),
    isTyped: true, //This is a control to reporting analiticas
    total: this.fb.control<number>(0.0, [
      Validators.required,
      Validators.min(0.01),
    ]),
    bill: this.fb.group({
      billNumber: this.fb.control<string>(''),
      issueDate: this.fb.control<Timestamp>(Timestamp.now()),
      subtotal: this.fb.control<number>(0.0, [
        Validators.required,
        Validators.min(0),
      ]),
      total: this.fb.control<number>(0.0, [
        Validators.required,
        Validators.min(0),
      ]),
      discount: this.fb.control<number>(0.0),
      sellerName: this.fb.control<string>(''),
      sellerEmail: this.fb.control<string>(''),
      items: this.fb.array<any>([]),
    }),
  });

  /*This is a helper form group to handle population
  of "items" form array control on SaleCreationFormGroup*/
  helperFormGroup = this.fb.group({
    itemCode: this.fb.control<string>('', [Validators.required]),
    itemPrice: this.fb.control<number>(0.0, [
      Validators.required,
      Validators.min(.0),
    ]),
    itemDiscount: this.fb.control<number>(0.0),
    itemQuantity: this.fb.control<number>(1, [
      Validators.required,
      Validators.min(1),
    ]),
    itemAmount: this.fb.control<number>(0.0, [
      Validators.required,
      Validators.min(0.01),
    ]),
  });

  withoutDetailMode = new FormControl(false);

  //This is a helper variable than provide global user
  userThanType = {
    displayName: '',
    email: '',
  };

  isCheckingCreation: boolean = false; // "..." spinner feedback

  constructor(
    private router: Router,
    private salesService: SalesService,
    private fb: FormBuilder,
    private auth: Auth
  ) {}

  /*TODO: Create an Alert Message preventing this view to be dismissed
  whenn click on "VOLVER" button if form is dirty...
  */
  ngOnInit() {
    this.auth.authStateReady();
    //Setting current User to add when save a Sale
    this.userThanType.displayName = this.auth.currentUser
      ?.displayName as string;

    this.userThanType.email = this.auth.currentUser?.email as string;
  }

  createNewSale() {
    const sale: SaleModelDTO = this.saleCreationFormGroup.value as any;

    sale.sellerName = this.userThanType.displayName;

    // This is if there is a bill, this bill must contain user name and email to history
    if (sale.bill) {
      sale.bill.sellerEmail = this.userThanType.email;
      sale.bill.sellerName = this.userThanType.displayName;
    }

    this.isCheckingCreation = true;

    console.log(sale);

    this.salesService
      .createSale(sale as any)
      .then((response) => {
        this.router.navigate(['billing', 'sales-management']);
        this.isCheckingCreation = false;
      })
      .catch((error: FirebaseError) => {
        console.error(error);
        this.isCheckingCreation = false;
      });
  }

  discardSale() {
    this.saleCreationFormGroup.reset();
  }

  goBack() {
    this.router.navigate(['billing', 'sales']);
  }

  validateInputErrors(formControlName: string): string {
    const validations = {
      required: 'El valor de este campo es requerido',
      email: 'Porfavor, ingrese un email válido ej: algo@algo.com',
      minlength: `El mínimo de caracteres permitidos es de 8 caracteres`,
      pattern:
        'El valor debe incluir: mayúsculas, minúsculas, números y símbolos ej: Basic2024$, *2024Basic',
      invalidCredentials: 'Las credenciales ingresadas son invalidas',
    };

    return validateFormControlErrors(
      formControlName,
      this.saleCreationFormGroup,
      validations
    );
  }

  addBillItem() {
    const items = this.saleCreationFormGroup.controls['bill']?.controls[
      'items'
    ] as FormArray;

    const newFormGroup = this.fb.group({
      itemCode: this.fb.control<string>('', [Validators.required]),
      itemPrice: this.fb.control<number>(0.0, [
        Validators.required,
        Validators.min(0.0),
      ]),
      itemDiscount: this.fb.control<number>(0.0),
      itemQuantity: this.fb.control<number>(1, [
        Validators.required,
        Validators.min(1),
      ]),
      itemAmount: this.fb.control<number>(0.0, [
        Validators.required,
        Validators.min(0.0),
      ]),
    });

    newFormGroup.setValue(this.helperFormGroup.value as any);
    this.helperFormGroup.reset();

    items.push(newFormGroup);
  }

  removeBillItem(index: number) {
    const items = this.saleCreationFormGroup.controls['bill']?.controls[
      'items'
    ] as FormArray;

    items.removeAt(index);
  }

  pickDatetime(){
  }
}
