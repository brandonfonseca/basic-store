import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { ReportingPage } from './reporting.page';
import { ReportingService } from './reporting.service';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';

describe('Caso de prueba A07', () => {
  let component: ReportingPage;
  let fixture: ComponentFixture<ReportingPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ReportingPage],
      providers: [ReportingService],
      imports: [
        SharedModule,
        IonicModule.forRoot(),
        provideFirebaseApp(() =>
          initializeApp({
            apiKey: 'AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs',
            authDomain: 'bs-bif.firebaseapp.com',
            projectId: 'bs-bif',
            storageBucket: 'bs-bif.appspot.com',
            messagingSenderId: '389025844621',
            appId: '1:389025844621:web:0e69097f5c33e9ea9381c4',
          })
        ),
        provideAuth(() => getAuth()),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('Correcta visualización de Componente de reportería', () => {
    expect(component).toBeTruthy();
  });
});
