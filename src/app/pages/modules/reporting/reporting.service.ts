import { Injectable } from '@angular/core';
import { Auth, idToken } from '@angular/fire/auth';
import {
  Timestamp,
  collection,
  getAggregateFromServer,
  getDocs,
  getFirestore,
  limit,
  orderBy,
  query,
  sum,
  where
} from '@angular/fire/firestore';
import { SaleModel } from '../billing/services/sale.type';


const months = [
  'jan',
  'feb',
  'mar',
  'apr',
  'may',
  'jun',
  'jul',
  'aug',
  'sep',
  'oct',
  'nov',
  'dec',
]


@Injectable({
  providedIn: 'root',
})
export class ReportingService {

  db = getFirestore(this.auth.app);

  constructor(private auth: Auth) { }

  //TODO: This must be performed by a Cloud Function because is very harder
  async getSalesByPeriod(period: 'today' | 'week' | 'month' | 'always' = 'today') {
    const from = this.#calculateDate(period)

    const q = query(
      collection(this.db, 'sales',),
      where('issueDate', '>=', from),
      orderBy('issueDate', 'desc')
    )

    const docs = (await getDocs(q)).docs;

    let doughnutValues = {
      morning: 0,
      afternoon: 0,
      night: 0
    };

    let lineValues = [
      0, // jan
      0, // feb
      0, // mar
      0, // apr
      0, // may
      0, // jun
      0, // jul
      0, // aug
      0, // sep
      0, // oct
      0, // nov
      0  // dec
    ]

    let barValues: {
      code: string,
      qty: number,
      name: string
    }[] = [] //Product List for best seller barValues

    docs.forEach(d => {

      //Calculating doughnut values
      const sale = d.data() as SaleModel;
      const hour = sale.issueDate.toDate().getHours()

      if (hour < 13) doughnutValues.morning += sale.total;
      else if (hour >= 13 && hour < 19) doughnutValues.afternoon += sale.total;
      else doughnutValues.night += sale.total;

      //Calculating yearly sales
      const month = sale.issueDate.toDate().getMonth();
      lineValues[month] += sale.total;

      //Calculating best seller prouducts

      sale.bill.items.forEach(i => {

        const productIndex = barValues.findIndex(p => p.code === i.itemCode)

        if (productIndex > -1) {

          const qty: number = Number(barValues[productIndex].qty);
          const newQty: number = Number(i.itemQuantity);
          const result: number = qty + newQty;
          barValues[productIndex].qty = Number(result);
          // console.log(result);


        } else {

          const newProduct = {
            code: i.itemCode,
            qty: i.itemQuantity,
            name: i.itemName
          }
          barValues.push(newProduct)

        }

      })


    })

    barValues.sort((a, b) => b.qty - a.qty) //order by descending
    barValues = barValues.slice(0, 5)
    // console.log(barValues);

    return {
      doughnutValues,
      barValues,
      lineValues
    }

  }


  /**Calculate a date starting from four periods
 *
 * @param period a period of time to start
 * @returns a start date from period selected
 */
  #calculateDate(period: 'today' | 'week' | 'month' | 'always' = 'today') {

    const date = new Date();

    //Date for today
    const dateFrom = {
      day: date.getDate(),
      month: date.getMonth(),
      year: date.getFullYear()
    }

    if (period === 'week') dateFrom.day -= date.getDay(); // First day of the week
    else if (period === 'month') dateFrom.day = 1; // first day of month
    else if (period === 'always') dateFrom.year = 1970; //From always

    const newDate = new Date();
    newDate.setDate(dateFrom.day);
    newDate.setMonth(dateFrom.month);
    newDate.setFullYear(dateFrom.year);

    const from = Timestamp
      .fromMillis(
        Date.parse(
          newDate.toDateString()
        )
      );

    return from;
  }

}
