import { Component, OnInit } from '@angular/core';
import { Chart, ChartItem, ChartType } from 'chart.js/auto';
import { ReportingService } from './reporting.service';

@Component({
  selector: 'p-reporting',
  templateUrl: './reporting.page.html',
  styleUrls: ['./reporting.page.scss'],
})
export class ReportingPage implements OnInit {
  circleChart!: Chart;
  lineChart!: Chart;
  barChart!: Chart;

  chartValues = {
    doughnutValues: {
      morning: 0.0,
      afternoon: 0.0,
      night: 0.0
    },
    barValues: [
      {
        code: '',
        qty: 0,
        name: 'Cargando...'
      },
      {
        code: '',
        qty: 0,
        name: 'Cargando...'
      },
      {
        code: '',
        qty: 0,
        name: 'Cargando...'
      },
      {
        code: '',
        qty: 0,
        name: 'Cargando...'
      },
      {
        code: '',
        qty: 0,
        name: 'Cargando...'
      },
    ],
    lineValues: [
      0, //jan
      0, //feb
      0, //mar
      0, //apr
      0, //may
      0, //jun
      0, //jul
      0, //aug
      0, //sep
      0, //oct
      0, //nov
      0  //dec
    ]
  }

  months = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'];

  constructor(
    private reportingService: ReportingService
  ) { }

  async ngOnInit() {

    await this.loadChartsData();

    this.createDoughnutChart();
    this.createBarChart();
    this.createLineChart();
  }

  createDoughnutChart() {
    const { morning, afternoon, night } = this.chartValues.doughnutValues;

    this.circleChart = new Chart('chart' as ChartItem, {
      type: 'doughnut' as ChartType,
      data: {
        labels: ['Mañana', 'Tarde', 'Noche'],
        datasets: [
          {
            label: 'Ventas por horario',
            data: [morning, afternoon, night],
            backgroundColor: [
              '#3dc2ff',
              '#5260ff',
              '#003fab',
            ],
          },
        ],
      },
    });
  }

  createPieChart() {

    this.circleChart = new Chart('pie-chart' as ChartItem, {
      type: 'pie' as ChartType,
      data: {
        labels: ['Red', 'Blue', 'Yellow'],
        xLabels: ['Red', 'Blue', 'Yellow'],
        datasets: [
          {
            label: 'Sales Dataset',
            data: [300, 50, 100],
            backgroundColor: [
              '#3dc2ff',
              '#5260ff',
              '#003fab',
            ],
          },
        ],
      },
    });
  }

  createBarChart() {
    this.barChart = new Chart('bar-chart' as ChartItem, {
      type: 'bar',
      data: {
        labels: this.chartValues.barValues.map(v => v.name),
        datasets: [
          {
            label: 'Productos con mayores ventas',
            data: this.chartValues.barValues.map(v => v.qty),
            backgroundColor: [
              '#003fab',
              '#3dc2ff',
              '#5260ff',
              '#222428',
              '#92949c'
            ],
          }
        ],
      },
      options: {
        responsive: true,
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });


  }

  createLineChart() {
    this.circleChart = new Chart('line-chart' as ChartItem, {
      type: 'line' as ChartType,
      data: {
        labels: this.months.slice(0, new Date().getMonth() + 1),
        datasets: [
          {
            label: `Ventas mensuales (${new Date().getFullYear()})`,
            data: this.chartValues.lineValues,
            backgroundColor: [
              '#003fab',
            ],
          },
        ],
      },
    });
  }

  async loadChartsData() {

    this.chartValues = await this.reportingService.getSalesByPeriod('always');
    // console.log(this.chartValues);

  }

  getTotalYearSales() {
    return this.chartValues.lineValues.reduce((amount, current) => amount + current)
  }
}
