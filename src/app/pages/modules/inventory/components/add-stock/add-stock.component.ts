import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { utilConstants } from 'src/util/util.constants';
import { ProductModel } from '../../services/products.type';
import { BillingService } from '../../../billing/services/billing.service';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'c-add-stock',
  templateUrl: './add-stock.component.html',
  styleUrls: ['./add-stock.component.scss'],
})
export class AddStockComponent {

  searchbarControl = new FormControl<string>('');
  stockToAdd = new FormControl<number>(0, [Validators.min(1)]);
  price = new FormControl<number>(0);
  productFound: ProductModel | null = null;

  noPhoto: string = utilConstants.noProductPhoto;

  constructor(
    private modalCtrl: ModalController,
    private toastCtrl: ToastController,
    private billingService: BillingService,
    private productService: ProductsService) { }

  async close() {
    await this.modalCtrl.dismiss();
  }

  async add() {
    console.log("Adding new Values");
    const stock = this.stockToAdd.value as number;
    const newPrice = this.price.value as number || 0;
    const id = this.productFound?.id as string;

    await this.productService.incrementStock(stock, id, newPrice)

    await this.modalCtrl.dismiss({
      value: 'Emitting values from modal',
      code: this.productFound?.code || '',
      stockAdded: stock,
      newPrice
    })

    const toast = await this.toastCtrl.create({
      header: "¡Hecho!",
      message: `Se añadieron ${this.stockToAdd.value as number} existencias al producto ${this.productFound?.name}`,
      color: 'primary',
      position: 'bottom',
      duration: 5000,
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel'
        }
      ]
    })

    toast.present();
  }

  async onSearch() {
    const value = this.searchbarControl.value as string;
    console.log(value);

    if (!value) {
      this.productFound = null;
      this.price.setValue(0)
      this.stockToAdd.setValue(0);
      return;
    }

    const data = (await this.billingService.findProductsByCodeOrName(value)).docs

    data.forEach(d => {
      const p: ProductModel = d.data() as ProductModel;
      p.id = d.id;

      this.productFound = p;
    })

    this.price.setValue(this.productFound?.detail.salePrice as number || 0)
    console.log(this.productFound);


  }

}
