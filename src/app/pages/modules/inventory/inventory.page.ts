import { Component, OnInit } from '@angular/core';
import MenubarListItemModel from 'src/app/shared/components/menubar/util/MenubarListItem.model';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.page.html',
  styleUrls: ['./inventory.page.scss'],
})
export class InventoryPage implements OnInit {
  moduleName: string = 'Inventario';
  menu: MenubarListItemModel[] = [
    {
      icon: 'pricetags',
      title: 'Gestionar Productos',
      description: '',
      route: 'inventory/products-management',
    },
  ];

  constructor() {}

  ngOnInit() {}
}
