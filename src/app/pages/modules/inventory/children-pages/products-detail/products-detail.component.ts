import { Component, OnInit } from '@angular/core';
import { FirebaseError } from '@angular/fire/app';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { map } from 'rxjs';
import { validateFormControlErrors } from 'src/app/shared/util/FormsValidations';
import { FileService } from '../../../administration/services/file.service';
import { ProductsService } from '../../services/products.service';
import { ProductModel, ProductModelDTO } from '../../services/products.type';
import { utilConstants } from 'src/util/util.constants';

@Component({
  selector: 'app-products-detail',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.scss'],
})
export class ProductsDetailComponent implements OnInit {
  productSelected: any = null;

  productFormGroup = new FormGroup({
    id: new FormControl<string>(''),
    photoURL: new FormControl<string>(''),
    name: new FormControl<string>('', [Validators.required]),
    code: new FormControl<string>('', [Validators.required]),
    metadata: new FormGroup({
      type: new FormControl<string>(''),
      size: new FormControl<number>(0),
      measurementUnit: new FormControl<string>(''),
    }),
    detail: new FormGroup({
      stock: new FormControl<number>(1),
      purchasePrice: new FormControl<number>(0.0),
      salePrice: new FormControl<number>(0.0),
      margin: new FormControl<number>(0.15),
    }),
  });

  disableEdition = true;
  noPhoto: string = utilConstants.noProductPhoto;

  picturePreview: string = '';
  isLoadingPicture: boolean = false;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private productService: ProductsService,
    private fileService: FileService
  ) { }

  ngOnInit() {
    this.getProductInfo();
  }

  toggleEdition() {
    this.disableEdition = !this.disableEdition;
    this.productFormGroup.setValue(this.productSelected);
    this.productFormGroup.markAsPristine();
  }

  getProductInfo() {
    this.activeRoute.params
      .pipe(map<Params, string>((params) => params['productId']))
      .subscribe(async (id) => {
        const data = await this.productService.readProductById(id);

        this.productSelected = data.data() as ProductModel;
        this.productSelected.id = id;

        this.picturePreview = this.productSelected.photoURL;
        this.productSelected.photoURL = ''; //Removing photoURL to avoid problem with input type file

        this.productFormGroup.setValue(this.productSelected);
      });
  }

  updateUserDetail() {
    const toUpdate = this.productFormGroup.value as ProductModelDTO;
    toUpdate.photoURL = this.picturePreview;

    this.productService.updateProduct(toUpdate).then((newUserData) => {
      console.log(newUserData);
      this.disableEdition = true;
    });
  }

  deleteProduct() {
    this.productService
      .deleteProduct(this.productSelected.id)
      .then(() => {
        this.router.navigate(['inventory', 'products-management']);
      })
      .catch((error: FirebaseError) => {
        console.error(error);
      });
  }

  validateInputErrors(formControlName: string): string {
    const validations = {
      required: 'El valor de este campo es requerido',
      email: 'Porfavor, ingrese un email válido ej: algo@algo.com',
      minlength: `El mínimo de caracteres permitidos es de 8 caracteres`,
      pattern:
        'El valor debe incluir: mayúsculas, minúsculas, números y símbolos ej: Basic2024$, *2024Basic',
      invalidCredentials: 'Las credenciales ingresadas son invalidas',
    };

    return validateFormControlErrors(
      formControlName,
      this.productFormGroup,
      validations
    );
  }

  openFileChooser(input: HTMLInputElement) {
    console.log('abriendo...');
    if (this.disableEdition) return;
    input.click();
  }

  /** Upload an image to firebase storage when an event is dispatched
   *
   * @param event
   */
  uploadImageToPreview(event: Event) {
    const fileEvent = event.target as HTMLInputElement;
    const files = fileEvent.files as FileList;

    console.log(files[0]);

    if (files.length == 0) {
      this.productFormGroup.setErrors({
        noFileSelectedError: true,
      });

      return;
    } else if (!files[0].type.includes('image/')) {
      this.productFormGroup.setErrors({
        fileTypeNotAllowed: true,
      });

      return;
    }

    const instant = Date.now();

    this.isLoadingPicture = true; //when image start to upload, init progress indicator

    this.fileService
      .uploadImage(`products/product${instant}`, files[0])
      .then(async (response) => {
        const downloadURL = await this.fileService.readImage(
          response.metadata.fullPath
        );

        this.picturePreview = downloadURL;
        this.isLoadingPicture = false;
      });
  }

  goBack() {
    this.router.navigate(['inventory', 'products-management']);
  }
}
