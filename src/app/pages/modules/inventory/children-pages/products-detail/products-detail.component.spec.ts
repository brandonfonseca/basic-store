import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { ProductsDetailComponent } from './products-detail.component';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { BillingService } from '../../../billing/services/billing.service';
import { ProductsService } from '../../services/products.service';
import { RouterModule } from '@angular/router';
import { getStorage, provideStorage } from '@angular/fire/storage';

describe('Caso de prueba A04', () => {
  let component: ProductsDetailComponent;
  let fixture: ComponentFixture<ProductsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductsDetailComponent],
      providers: [ProductsService, BillingService, ModalController],
      imports: [
        ReactiveFormsModule,
        RouterModule.forRoot([]),
        IonicModule.forRoot(),
        provideFirebaseApp(() =>
          initializeApp({
            apiKey: 'AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs',
            authDomain: 'bs-bif.firebaseapp.com',
            projectId: 'bs-bif',
            storageBucket: 'bs-bif.appspot.com',
            messagingSenderId: '389025844621',
            appId: '1:389025844621:web:0e69097f5c33e9ea9381c4',
          })
        ),
        provideAuth(() => getAuth()),
        provideStorage(() => getStorage())
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('Correcta Visualización de formulario para actualizar Producto', () => {
    expect(component).toBeTruthy();
  });
});
