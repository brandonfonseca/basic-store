import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { ProductModel } from '../../services/products.type';
import { utilConstants } from 'src/util/util.constants';
import { FormControl } from '@angular/forms';
import { BillingService } from '../../../billing/services/billing.service';
import { ModalController } from '@ionic/angular';
import { AddStockComponent } from '../../components/add-stock/add-stock.component';
import { AttachFileComponent } from 'src/app/shared/components/attach-file/attach-file.component';

@Component({
  selector: 'app-products-management',
  templateUrl: './products-management.component.html',
  styleUrls: ['./products-management.component.scss'],
})
export class ProductsManagementComponent implements OnInit {
  products: ProductModel[] = [];
  noPhoto: string = utilConstants.noProductPhoto;

  searchbarControl = new FormControl<string>('');
  elementsPerPage = new FormControl(7)
  currentPage = 1
  totalPages = 1

  constructor(
    private router: Router,
    private productService: ProductsService,
    private billingService: BillingService,
    private modalCtrl: ModalController
  ) { }

  async ngOnInit() {
    // await this.onUploadFile();
    await this.readProductsByPagination('', 'asc');
  }

  viewProductDetail(productId: string) {
    this.router.navigate(['inventory', 'products-detail', productId]);
  }

  goToCreationProductView() {
    this.router.navigate(['inventory', 'products-creation']);
  }

  /**
   * @deprecated
   */
  readProducts() {
    this.productService.readAllProducts()

      .then((data) => {
        data.forEach((doc) => {
          // console.log(doc.data());
          const product = doc.data() as ProductModel;
          product.id = doc.id;
          this.products.push(product);
        });
      });
  }

  async paginate(direction: 'next' | 'back') {

    let refCode = '';

    if (direction === 'back') {
      this.currentPage -= 1;

      refCode = this.products[0].code as string;

      await this.readProductsByPagination(refCode, 'desc')

    } else {
      this.currentPage += 1;

      refCode = this.products[
        this.products.length - 1
      ].code as string;

      await this.readProductsByPagination(refCode, 'asc')

    }

  }

  async readPaginationData() {

    const totalProducts = await this.productService.countProducts();
    const elementsToShow = totalProducts / (this.elementsPerPage.value as number);//this could be a decimal result
    const pages = Math.round(
      elementsToShow
    )

    this.totalPages = pages <= elementsToShow ? pages : pages - 1;

    if (pages < elementsToShow) this.totalPages = pages + 1;
    else if (pages > elementsToShow) this.totalPages = pages - 1;
    else this.totalPages = pages;
  }

  async readProductsByPagination(code: string, direction: 'asc' | 'desc') {
    const elementsToShow = this.elementsPerPage.value as number;

    let pp = await this.productService.readAllProductsByPagination(elementsToShow, code, direction)

    if (direction === 'desc') pp = pp.reverse();

    const prds: ProductModel[] = []
    pp.forEach(p => {

      const productFound: ProductModel = p.data() as ProductModel;
      productFound.id = p.id;

      prds.push(productFound)

    })

    this.products = prds;
    await this.readPaginationData();
  }

  async findProductByCriteria() {
    const value = this.searchbarControl.value as string;
    this.currentPage = 1;
    this.totalPages = 1;

    const products = await this.billingService.findProductsByCodeOrName(value);

    if (value.length < 1) {
      this.readProductsByPagination('', 'asc')
    }
    else {
      // await this.readPaginationData();
      this.products = products.docs.map(p => p.data() as ProductModel);
    }

  }

  async onAddStock() {
    const modal = await this.modalCtrl.create({
      component: AddStockComponent
    });

    modal.present();

    const { data } = await modal.onWillDismiss();

    //Updating product if is listed
    const productToUpdate = this.products.find(p => p.code === data.code);
    if (productToUpdate) productToUpdate.detail.stock += data.stockAdded;
  }

  async onUploadFile() {

    const fileModal = await this.modalCtrl.create({
      component: AttachFileComponent,
    })

    await fileModal.present();

  }
}
