import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule, ModalController } from '@ionic/angular';

import { ProductsManagementComponent } from './products-management.component';
import { ProductsService } from '../../services/products.service';
import { BillingService } from '../../../billing/services/billing.service';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { ReactiveFormsModule } from '@angular/forms';

describe('Casos de prueba A01 y A03', () => {
  let component: ProductsManagementComponent;
  let fixture: ComponentFixture<ProductsManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductsManagementComponent],
      providers: [ProductsService, BillingService, ModalController],
      imports: [
        ReactiveFormsModule,
        IonicModule.forRoot(),
        provideFirebaseApp(() =>
          initializeApp({
            apiKey: 'AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs',
            authDomain: 'bs-bif.firebaseapp.com',
            projectId: 'bs-bif',
            storageBucket: 'bs-bif.appspot.com',
            messagingSenderId: '389025844621',
            appId: '1:389025844621:web:0e69097f5c33e9ea9381c4',
          })
        ),
        provideAuth(() => getAuth()),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Caso A01: Correcta visualización de componente de búsqueda de productos', () => {
    let element = fixture.nativeElement.querySelectorAll('ion-searchbar');

    expect(element.length).toBe(1);
  });

  it('Caso A03: Correcta Visualización de Productos en “Gestión de productos”', () => {
    expect(component).toBeTruthy();
  });

});
