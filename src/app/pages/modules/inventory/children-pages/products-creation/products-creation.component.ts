import { Component, OnInit } from '@angular/core';
import { FirebaseError } from '@angular/fire/app';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { validateFormControlErrors } from 'src/app/shared/util/FormsValidations';
import { utilConstants } from 'src/util/util.constants';
import { FileService } from '../../../administration/services/file.service';
import { ProductsService } from '../../services/products.service';
import { ProductModelDTO } from '../../services/products.type';

@Component({
  selector: 'app-products-creation',
  templateUrl: './products-creation.component.html',
  styleUrls: ['./products-creation.component.scss'],
})
export class ProductsCreationComponent implements OnInit {
  productCreationFormGroup = new FormGroup({
    photoURL: new FormControl<string>(''),
    name: new FormControl<string>('', [Validators.required]),
    code: new FormControl<string>('', [Validators.required]),
    metadata: new FormGroup({
      type: new FormControl<string>(''),
      size: new FormControl<number>(0),
      measurementUnit: new FormControl<string>(''),
    }),
    detail: new FormGroup({
      stock: new FormControl<number>(1),
      purchasePrice: new FormControl<number>(0.0),
      salePrice: new FormControl<number>(0.0),
      margin: new FormControl<number>(15),
    }),
  });

  pictureSelected: File | null = null;

  picturePreview: string = '';
  noPhoto: string = utilConstants.noProductPhoto;

  isLoadingPicture: boolean = false;
  isCheckingCreation: boolean = false; // "..." spinner feedback

  constructor(
    private router: Router,
    private productService: ProductsService,
    private fileService: FileService
  ) { }

  /*TODO: Create an Alert Message preventing this view to be dismissed
  whenn click on "VOLVER" button if form is dirty...
  */
  ngOnInit() { }

  openFileChooser(input: HTMLInputElement) {
    input.click();
  }

  /** Upload an image to firebase storage when an event is dispatched
   *
   * @param event
   */
  uploadImageToPreview(event: Event) {
    const fileEvent = event.target as HTMLInputElement;
    const files = fileEvent.files as FileList;

    if (files.length == 0) {
      this.productCreationFormGroup.setErrors({
        noFileSelectedError: true,
      });

      return;
    } else if (!files[0].type.includes('image/')) {
      this.productCreationFormGroup.setErrors({
        fileTypeNotAllowed: true,
      });

      return;
    }

    const instant = Date.now();

    this.isLoadingPicture = true; //when image start to upload, init progress indicator

    this.fileService
      .uploadImage(`products/product-${instant}`, files[0])
      .then(async (response) => {
        const downloadURL = await this.fileService.readImage(
          response.metadata.fullPath
        );

        this.picturePreview = downloadURL;
        this.isLoadingPicture = false;
      });
  }

  createNewUser() {
    const product = this.productCreationFormGroup.value as ProductModelDTO;

    product.photoURL = this.picturePreview || this.noPhoto;
    this.isCheckingCreation = true;

    console.log(product);

    this.productService
      .createProduct(product)
      .then((response) => {
        this.router.navigate(['inventory', 'products-management']);
        this.isCheckingCreation = false;
      })
      .catch((error: FirebaseError) => {
        console.error(error);
        this.isCheckingCreation = false;
      });
  }

  goBack() {
    this.router.navigate(['inventory', 'products-management']);
  }

  discardUser() {
    this.productCreationFormGroup.reset();
    this.picturePreview = '';
  }

  validateInputErrors(formControlName: string): string {
    const validations = {
      required: 'El valor de este campo es requerido',
      email: 'Porfavor, ingrese un email válido ej: algo@algo.com',
      minlength: `El mínimo de caracteres permitidos es de 8 caracteres`,
      pattern:
        'El valor debe incluir: mayúsculas, minúsculas, números y símbolos ej: Basic2024$, *2024Basic',
      invalidCredentials: 'Las credenciales ingresadas son invalidas',
    };

    return validateFormControlErrors(
      formControlName,
      this.productCreationFormGroup,
      validations
    );
  }

  onCalculateSalesPrice(source: 'price' | 'cost' | 'margin') {

    const controls = this.productCreationFormGroup.controls['detail'];

    let cost = controls.get('purchasePrice')?.value || 0;
    let price = controls.get('salePrice')?.value || 0;
    let margin = controls.get('margin')?.value || 0;

    // When calculating increasing the price input, value to calculate is "margin"
    if (source === 'price') {
      console.log('Calculating price... by price')
      const priceDiff = (price - cost)
      const x = priceDiff * 100 / cost;
      margin = x || 0;
    }
    // When calculating increasing the price input, value to calculate is "margin"
    else {
      console.log('Calculating price... by margin');

      const saveCost = cost > 0 ? cost : 1; //To validate if cost is 0

      const x = saveCost * (margin / 100) + saveCost;
      price = x || 0;
    }

    controls.get('purchasePrice')?.setValue(cost);
    controls.get('salePrice')?.setValue(price)
    controls.get('margin')?.setValue(margin)
  }
}
