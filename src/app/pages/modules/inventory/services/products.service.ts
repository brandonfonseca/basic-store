import { Injectable } from '@angular/core';
import { ProductModel } from './products.type';
import { Auth } from '@angular/fire/auth';
import {
  addDoc,
  collection,
  getCountFromServer,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  getFirestore,
  updateDoc,
  query,
  startAfter,
  limit,
  orderBy,
  increment,
  setDoc,
} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  db = getFirestore(this.auth.app);

  constructor(private auth: Auth) { }

  async readAllProducts() {
    const products = (await getDocs(collection(this.db, 'products'))).docs;

    return products;
  }

  async readAllProductsByPagination(elementsPerPage: number, refCode: string, direction: 'asc' | 'desc') {

    const q = refCode.length < 1 ? query(
      collection(this.db, 'products'),
      limit(elementsPerPage),
      orderBy('code', direction)
    ) :
      query(
        collection(this.db, 'products'),
        limit(elementsPerPage),
        orderBy('code', direction),
        startAfter(refCode)
      )

    const products = (await getDocs(q)).docs;

    return products;
  }



  readProductById(id: string) {
    return getDoc(doc(this.db, 'products', id));
  }

  createProduct(newProduct: ProductModel) {
    return addDoc(collection(this.db, 'products'), newProduct);
  }

  updateProduct(changedProduct: ProductModel) {
    return updateDoc(doc(this.db, 'products', changedProduct.id), {
      ...changedProduct,
    });
  }

  incrementStock(addition: number, id: string, price: number) {

    return updateDoc(doc(this.db, 'products', id), {
      "detail.stock": increment(addition),
      "detail.salePrice": price
    });
  }

  deleteProduct(id: string) {
    return deleteDoc(doc(this.db, 'products', id));
  }

  async countProducts() {

    const productsCount = await getCountFromServer(
      collection(this.db, 'products')
    )

    return productsCount.data().count

  }
}
