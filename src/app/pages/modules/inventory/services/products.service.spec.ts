import { TestBed } from '@angular/core/testing';

import { ProductsService } from './products.service';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';

describe('Caso de prueba A05', () => {
  let service: ProductsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        provideFirebaseApp(() =>
          initializeApp({
            apiKey: 'AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs',
            authDomain: 'bs-bif.firebaseapp.com',
            projectId: 'bs-bif',
            storageBucket: 'bs-bif.appspot.com',
            messagingSenderId: '389025844621',
            appId: '1:389025844621:web:0e69097f5c33e9ea9381c4',
          })
        ),
        provideAuth(() => getAuth()),
      ],
    });
    service = TestBed.inject(ProductsService);
  });

  it('Debe contener un método para la funcionalidad de eliminación de productos', () => {
    expect(service.deleteProduct).toBeDefined();
  });
});
