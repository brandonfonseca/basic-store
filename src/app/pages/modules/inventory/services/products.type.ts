export interface ProductModel {
  id: string;
  photoURL: string;
  name: string;
  code: string;
  metadata: {
    type: string;
    size: number;
    measurementUnit: string;
  };
  detail: ProductDetailModel;
}

export interface ProductDetailModel {
  code: string;
  stock: number;
  purchasePrice: number;
  salePrice: number;
  margin: number;
}

export interface ProductModelDTO extends ProductModel, Omit<string, 'id'> {}

export interface ProductOnInvoiceModel extends ProductModel {
  quantity: number;
  totalValue: number;
}
