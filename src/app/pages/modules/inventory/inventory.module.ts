import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InventoryPageRoutingModule } from './inventory-routing.module';

import { InventoryPage } from './inventory.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductsManagementComponent } from './children-pages/products-management/products-management.component';
import { ProductsCreationComponent } from './children-pages/products-creation/products-creation.component';
import { ProductsDetailComponent } from './children-pages/products-detail/products-detail.component';
import { AddStockComponent } from './components/add-stock/add-stock.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    InventoryPageRoutingModule,
  ],
  declarations: [
    InventoryPage,
    ProductsManagementComponent,
    ProductsCreationComponent,
    ProductsDetailComponent,
    AddStockComponent
  ],
})
export class InventoryPageModule { }
