import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InventoryPage } from './inventory.page';
import { ProductsManagementComponent } from './children-pages/products-management/products-management.component';
import { ProductsCreationComponent } from './children-pages/products-creation/products-creation.component';
import { ProductsDetailComponent } from './children-pages/products-detail/products-detail.component';
import { WelcomeComponent } from 'src/app/shared/components/welcome/welcome.component';

const routes: Routes = [
  {
    path: '',
    component: InventoryPage,
    children: [
      {
        path: 'welcome',
        component: WelcomeComponent,
        data: {
          module: 'inventory',
        }
      },
      {
        path: 'products-management',
        component: ProductsManagementComponent,
      },
      {
        path: 'products-detail/:productId',
        component: ProductsDetailComponent,
      },
      {
        path: 'products-creation',
        component: ProductsCreationComponent,
      },
      {
        path: '**',
        redirectTo: 'welcome',
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InventoryPageRoutingModule { }
