import { Component, OnInit } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { adminGuard } from 'src/app/guards/role.guard';


@Component({
  selector: 'p-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  modules: any[] = [];
  userPicture = '';
  userName = '';
  noPhoto: string =
    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png';

  constructor(
    private router: Router,
    private auth: Auth,
    private authService: AuthService
  ) { }

  async ngOnInit() {
    this.modules = [];

    this.modules.unshift(
      {
        name: 'Facturación',
        description:
          'Módulo para gestionar y ejecutar la facturación del sistema así como las ventas y las facturas',
        icon: 'cube',
        route: 'billing',
      },
      {
        name: 'Inventario',
        description:
          'Módulo para la gestión de productos y existencias en el sistema',
        icon: 'flask',
        route: 'inventory',
      }
    )
    await this.loadUserData();
  }

  goToAccount() {
    this.router.navigate(['account']);
  }

  async loadUserData() {
    await this.auth.authStateReady();

    this.userPicture = this.auth.currentUser?.photoURL as string;
    this.userName = this.auth.currentUser?.displayName as string;

    await this.loadAdminModules();
  }

  async loadAdminModules() {

    const tokenId = await this.auth.currentUser?.getIdTokenResult();
    const roles = tokenId?.claims['roles'] as string[];

    if (roles.includes('admin')) {
      this.modules.unshift(
        {
          name: 'Administración',
          description: 'Módulo administrativo para gestión del sistema en general',
          icon: 'settings',
          route: 'administration',
        },
        {
          name: 'Reportería y analíticas',
          description:
            'Módulo para la recopilación de información y toma de decisiones',
          icon: 'analytics',
          route: 'reporting',
        });
    }


  }

  goTo(route: string) {
    this.router.navigate(route.split('/'));
  }

  logout() {
    //TODO: Make a loader to indicate closing session
    this.authService.logout();
    this.router.navigate(['auth', 'login']);
  }
}
