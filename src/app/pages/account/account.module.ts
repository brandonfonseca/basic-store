import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountPageRoutingModule } from './account-routing.module';

import { AccountPage } from './account.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ManageAccountComponent } from './children-pages/manage-account/manage-account.component';
import { ChangePassFormComponent } from './children-pages/change-pass-form/change-pass-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AccountPageRoutingModule,
    SharedModule,
  ],
  declarations: [AccountPage, ManageAccountComponent, ChangePassFormComponent],
})
export class AccountPageModule {}
