import { Component, OnInit } from '@angular/core';
import { Auth, User } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'p-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  menuListItems: any[] = [
    {
      icon: 'person-circle',
      title: 'Cuenta de usuarios',
      route: 'account/manage',
    },
    {
      icon: 'notifications',
      title: 'Notificaciones',
      route: 'notifications',
    },
  ];
  currentUser: User | null = null;
  noPhoto =
    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png';

  constructor(private router: Router, private auth: Auth) {}

  async ngOnInit() {
    await this.auth.authStateReady();
    this.currentUser = this.auth.currentUser;
  }

  goToDirection(route: string) {
    if (route.length === 0) return;
    this.router.navigateByUrl(route);
  }

  goBack() {
    this.router.navigate(['home']);
  }
}
