import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { validateFormControlErrors } from 'src/app/shared/util/FormsValidations';
import { FileService } from '../../../modules/administration/services/file.service';
import { UsersService } from '../../../modules/administration/services/users.service';
import { UsersModel } from '../../../modules/administration/services/users.type';
import { Auth, User } from '@angular/fire/auth';
import { AuthService } from 'src/app/pages/auth/auth.service';
import { utilConstants } from 'src/util/util.constants';
import { ModalController } from '@ionic/angular';
import { ChangePassFormComponent } from '../change-pass-form/change-pass-form.component';

@Component({
  selector: 'c-manage-account',
  templateUrl: './manage-account.component.html',
  styleUrls: ['./manage-account.component.scss'],
})
export class ManageAccountComponent implements OnInit {
  userSelected: any = null;

  userDetailFormGroup = new FormGroup({
    id: new FormControl(''),
    photoURL: new FormControl(''),
    names: new FormControl('', [Validators.required]),
    lastNames: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phoneNumber: new FormControl('', [Validators.minLength(8)]),
    identification: new FormControl('', [
      Validators.required,
      //TODO: Validate february and the other months with 31 and 30 days per month
      // Validators.pattern(
      //   '^([0-9]{3})-([0-2][0-9]|3[0-1])([0-1]{1}[0-2]{1})([0-9]{2})-([0-9]{4})([A-Za-z])$'
      // ),
    ]),
    address: new FormControl(''),
    roles: new FormControl<string[]>({ value: [], disabled: false }, [
      Validators.required,
      Validators.minLength(1),
    ]),
  });

  validators = [
    Validators.required,
    Validators.min(8),
    Validators.pattern(utilConstants.regexPatterns.password),
  ];

  changePasswordForm = new FormGroup({
    newPassword: new FormControl('', this.validators),
    verifyPassword: new FormControl('', this.validators),
  });

  disableEdition = true;
  noPhoto: string =
    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png';

  picturePreview: string = '';
  isLoadingPicture: boolean = false;

  constructor(
    private router: Router,
    private auth: Auth,
    private modalCtrl: ModalController,
    private userService: UsersService,
    private authService: AuthService,
    private fileService: FileService
  ) {}

  async ngOnInit() {
    await this.auth.authStateReady();

    this.getUserDetail();
  }

  toggleEdition() {
    this.disableEdition = !this.disableEdition;
    this.userDetailFormGroup.setValue(this.userSelected);
    this.userDetailFormGroup.markAsPristine();
  }

  //TODO: Solve this, user detail is not loading
  async getUserDetail() {
    const id = this.auth.currentUser?.uid as string;
    const data = await this.userService.readUserWithDetailById(id);

    this.userSelected = data.data() as UsersModel;
    this.userSelected.id = id;

    this.picturePreview = this.userSelected.photoURL;
    this.userSelected.photoURL = ''; //Removing photoURL to avoid problem with input type file

    this.userDetailFormGroup.setValue(this.userSelected);
  }

  updateUserDetail() {
    const toUpdate = this.userDetailFormGroup.value as UsersModel;
    toUpdate.photoURL = this.picturePreview;

    this.userService.modifiedUser(toUpdate).then((newUserData) => {
      console.log(newUserData);
      this.disableEdition = true;
    });
  }

  async changePassword() {
    const form = this.changePasswordForm.value;

    if (form.newPassword != form.verifyPassword) return;

    await this.authService.changePassword(
      this.auth.currentUser as User,
      form.newPassword as string
    );
  }

  validateInputErrors(formControlName: string): string {
    const validations = {
      required: 'El valor de este campo es requerido',
      email: 'Porfavor, ingrese un email válido ej: algo@algo.com',
      minlength: `El mínimo de caracteres permitidos es de 8 caracteres`,
      pattern:
        'El valor debe incluir: mayúsculas, minúsculas, números y símbolos ej: Basic2024$, *2024Basic',
      invalidCredentials: 'Las credenciales ingresadas son invalidas',
    };

    return validateFormControlErrors(
      formControlName,
      this.userDetailFormGroup,
      validations
    );
  }

  openFileChooser(input: HTMLInputElement) {
    if (this.disableEdition) return;
    input.click();
  }

  /** Upload an image to firebase storage when an event is dispatched
   *
   * @param event
   */
  uploadImageToPreview(event: Event) {
    const fileEvent = event.target as HTMLInputElement;
    const files = fileEvent.files as FileList;

    console.log(files[0]);

    if (files.length == 0) {
      this.userDetailFormGroup.setErrors({
        noFileSelectedError: true,
      });

      return;
    } else if (!files[0].type.includes('image/')) {
      this.userDetailFormGroup.setErrors({
        fileTypeNotAllowed: true,
      });

      return;
    }

    const instant = Date.now();

    this.isLoadingPicture = true; //when image start to upload, init progress indicator

    this.fileService
      .uploadImage(`perfiles/profile${instant}`, files[0])
      .then(async (response) => {
        const downloadURL = await this.fileService.readImage(
          response.metadata.fullPath
        );

        this.picturePreview = downloadURL;
        this.isLoadingPicture = false;
      });
  }

  async openChangePasswordModal() {
    const modal = await this.modalCtrl.create({
      component: ChangePassFormComponent,
    });

    modal.present();
  }

  goBack() {
    this.router.navigate(['administration', 'users-management']);
  }
}
