import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/pages/auth/auth.service';

@Component({
  selector: 'c-change-pass-form',
  templateUrl: './change-pass-form.component.html',
  styleUrls: ['./change-pass-form.component.scss'],
})
export class ChangePassFormComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private modalCtrl: ModalController,
    private toastCtrl: ToastController
  ) {}

  ngOnInit() {}

  async onUpdatePassword() {
    const toast = await this.toastCtrl.create({
      message: '¡Se ha actualizado su contraseña con éxito!',
      color: 'success',
      duration: 4000,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          icon: 'checkmark',
        },
      ],
    });

    await toast.present();

    this.modalCtrl.dismiss();
  }

  onCancel() {
    this.modalCtrl.dismiss();
  }
}
