import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { validateFormControlErrors } from 'src/app/shared/util/FormsValidations';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'c-reset-pass-form',
  templateUrl: './reset-pass-form.component.html',
  styleUrls: ['./reset-pass-form.component.scss'],
})
export class ResetPassFormComponent {

  passResetForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  })

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  validateInputErrors(formControlName: string): string {
    const validations = {
      required: 'El valor de este campo es requerido',
      email: 'Porfavor, ingrese un email válido ej: algo@algo.com'
    };

    return validateFormControlErrors(
      formControlName,
      this.passResetForm,
      validations
    );
  }

  sendEmail() {
    const { email } = this.passResetForm.value;

    this.authService.requestResetPasswordEmail(email as string)
      .then(_ => {

        console.log('Email Sent');

      }).catch(err => {
        console.log('An Error has ocurr');

      })
  }

  goBack(){
    this.router.navigateByUrl('auth/login')
  }
}
