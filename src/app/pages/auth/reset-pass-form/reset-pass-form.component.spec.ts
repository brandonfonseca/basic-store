import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResetPassFormComponent } from './reset-pass-form.component';
import { AuthService } from '../auth.service';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

describe('Caso de prueba A08', () => {
  let component: ResetPassFormComponent;
  let fixture: ComponentFixture<ResetPassFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ResetPassFormComponent],
      providers: [AuthService],
      imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forRoot([]),
        IonicModule.forRoot(),
        provideFirebaseApp(() =>
          initializeApp({
            apiKey: 'AIzaSyBsdGUQUahBaxJMjqNl-p1Q6F02gugJMGs',
            authDomain: 'bs-bif.firebaseapp.com',
            projectId: 'bs-bif',
            storageBucket: 'bs-bif.appspot.com',
            messagingSenderId: '389025844621',
            appId: '1:389025844621:web:0e69097f5c33e9ea9381c4',
          })
        ),
        provideAuth(() => getAuth()),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetPassFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Correcta visualización de formulario para reestablecer contraseña', () => {
    expect(component).toBeTruthy();
  });
});
