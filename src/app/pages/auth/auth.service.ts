import { Injectable } from '@angular/core';
import { FirebaseError } from '@angular/fire/app';
import {
  Auth,
  UserCredential,
  signInWithEmailAndPassword,
  confirmPasswordReset,
  updatePassword,
  signOut,
  User,
  sendPasswordResetEmail
} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private auth: Auth) { }

  async login(username: string, password: string): Promise<UserCredential> {
    const res = await signInWithEmailAndPassword(this.auth, username, password);

    return res;
  }

  async resetPassword(newPassword: string, codes: { oobCode: string }) {

    try {
      await confirmPasswordReset(this.auth, codes.oobCode, newPassword);
    } catch (error) {
      console.log(error as FirebaseError);
    }

  }

  async requestResetPasswordEmail(email: string){
    return sendPasswordResetEmail(this.auth, email);
  }

  async changePassword(currentUser: User, newPassword: string) {
    await updatePassword(currentUser, newPassword);
  }

  logout(): Promise<void> {
    return signOut(this.auth);
  }
}
