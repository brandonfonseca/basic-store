import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { utilConstants } from 'src/util/util.constants';
import { AuthService } from '../auth.service';
import { validateFormControlErrors } from 'src/app/shared/util/FormsValidations';

@Component({
  selector: 'c-pass-recovery',
  templateUrl: './pass-recovery.component.html',
  styleUrls: ['./pass-recovery.component.scss'],
})
export class PassRecoveryComponent implements OnInit {

  mode: string = '';
  oobCode: string = '';
  apiKey: string = '';

  validators = [
    Validators.required,
    Validators.min(8),
    Validators.pattern(
      utilConstants.regexPatterns.password
    )];


  changePasswordForm = new FormGroup({
    newPassword: new FormControl('', this.validators),
    verifyPassword: new FormControl('', this.validators)
  })

  constructor(
    private activeRoute: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit() {
    //Setting route recovery queryParams
    this.activeRoute.queryParams.subscribe(qp => {
      this.mode = qp['mode']
      this.oobCode = qp['oobCode']
      this.apiKey = qp['apiKey']
    })
  }

  verifyPass() {
    const form = this.changePasswordForm.value;

    if (form.newPassword != form.verifyPassword) {
      this.changePasswordForm.get('verifyPassword')?.setErrors({
        passNotMatch: true
      })
    };
  }

  validateInputErrors(formControlName: string): string {
    const validations = {
      required: 'El valor de este campo es requerido',
      minlength: `El mínimo de caracteres permitidos es de 8 caracteres`,
      pattern:
        'El valor debe incluir: mayúsculas, minúsculas, números y símbolos ej: Basic2024$, *2024Basic',
      passNotMatch: 'Las contraseñas ingresadas no coinciden, porfavor ingresa la misma contraseña en ambos campos'
    };

    return validateFormControlErrors(
      formControlName,
      this.changePasswordForm,
      validations
    );
  }

  async resetPassword() {

    const form = this.changePasswordForm.value;
    if (form.newPassword != form.verifyPassword) return;

    await this.authService.resetPassword(form.newPassword as string, { oobCode: this.oobCode })

    console.log('Contraseña Actualizada con éxito');

  }
}
