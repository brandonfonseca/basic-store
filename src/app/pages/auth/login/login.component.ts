import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IonButton, IonIcon, IonInput } from '@ionic/angular';
import { AuthService } from '../auth.service';
import { validateFormControlErrors } from 'src/app/shared/util/FormsValidations';

@Component({
  selector: 'c-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.pattern(
        '^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[^a-zA-Z0-9]).{8,}$'
      ),
    ]),
  });

  isPasswordDirty: boolean; //Used for show/hide show password button
  isCheckingLogin: boolean; // "..." spinner feedback

  constructor(private router: Router, private authService: AuthService) {
    this.isPasswordDirty = false;
    this.isCheckingLogin = false;
  }

  login(button: IonButton) {
    //Setting up visual change to indicate session started
    button.disabled = true;
    this.isCheckingLogin = true;

    const { username, password } = this.loginForm.value;

    this.authService
      .login(username, password)
      .then((fireResponse) => {
        this.loginForm.reset();
        this.router.navigate(['home']);
      })
      .catch((fireError) => {
        console.log(fireError);
        //Reseting fields to their init values
        button.disabled = false;
        this.isCheckingLogin = false;
        this.loginForm.get('username')?.setErrors({ invalidCredentials: true });
        this.loginForm.get('password')?.setErrors({ invalidCredentials: true });
        console.log(this.loginForm);
      });
  }

  validateInputErrors(formControlName: string): string {
    const validations = {
      required: 'El valor de este campo es requerido',
      email: 'Porfavor, ingrese un email válido ej: algo@algo.com',
      minlength: `El mínimo de caracteres permitidos es de 8 caracteres`,
      pattern:
        'El valor debe incluir: mayúsculas, minúsculas, números y símbolos ej: Basic2024$, *2024Basic',
      invalidCredentials: 'Las credenciales ingresadas son invalidas',
    };

    return validateFormControlErrors(
      formControlName,
      this.loginForm,
      validations
    );
  }

  showPassword(element: IonInput, icon: IonIcon) {
    const { type } = element;
    const { name } = icon;

    element.type = type === 'password' ? 'text' : 'password';
    icon.name = name === 'eye' ? 'eye-off' : 'eye';
  }

  showHideShowPassButton() {
    this.isPasswordDirty = this.loginForm.get('password')?.value as boolean;
  }

  goTo(route: string){
    this.router.navigateByUrl(route)
  }
}
