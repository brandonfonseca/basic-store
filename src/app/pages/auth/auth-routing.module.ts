import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthPage } from './auth.page';
import { LoginComponent } from './login/login.component';
import { PassRecoveryComponent } from './pass-recovery/pass-recovery.component';
import { ResetPassFormComponent } from './reset-pass-form/reset-pass-form.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login',
  },
  {
    path: '',
    component: AuthPage,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'pass-recovery',
        pathMatch: 'prefix',
        component: PassRecoveryComponent,
      },
      {
        path: 'reset',
        component: ResetPassFormComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthPageRoutingModule { }
