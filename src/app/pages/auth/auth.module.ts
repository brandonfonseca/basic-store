import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthPageRoutingModule } from './auth-routing.module';

import { AuthPage } from './auth.page';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { PassRecoveryComponent } from './pass-recovery/pass-recovery.component';
import { ResetPassFormComponent } from './reset-pass-form/reset-pass-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AuthPageRoutingModule
  ],
  declarations: [AuthPage, LoginComponent, PassRecoveryComponent, ResetPassFormComponent],
  providers: [AuthService],
})
export class AuthPageModule { }
